<?php

declare(strict_types=1);

namespace Thrustbit\Security\Domain\Role\Values;

use Thrustbit\Security\Application\Values\SecurityValue;
use Thrustbit\Security\Domain\Role\RoleSecurity as RoleContract;

class RoleValue implements RoleContract, SecurityValue
{
    /**
     * @var string
     */
    private $role;

    public function __construct(string $role)
    {
        $this->role = $role;
    }

    public function getRole(): string
    {
        return $this->role;
    }

    public function __toString(): string
    {
        return $this->role;
    }

    public function sameValueAs(SecurityValue $aValue): bool
    {
        return $aValue instanceof RoleValue
            && $this->role === $aValue->getRole();
    }
}