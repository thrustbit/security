<?php

declare(strict_types=1);

namespace Thrustbit\Security\Domain\Role\Values;

use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

class SwitchUserRole extends RoleValue
{
    /**
     * @var Tokenable
     */
    private $source;

    public function __construct(string $role, Tokenable $source)
    {
        parent::__construct($role);

        $this->source = $source;
    }

    public function source(): Tokenable
    {
        return $this->source;
    }
}