<?php

declare(strict_types=1);

namespace Thrustbit\Security\Domain\Role;

interface RoleHierarchy
{
    public function getReachableRoles(array $roles);
}