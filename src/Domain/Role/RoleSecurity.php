<?php

declare(strict_types=1);

namespace Thrustbit\Security\Domain\Role;

interface RoleSecurity
{
    public function getRole(): string;

    public function __toString();
}