<?php

declare(strict_types=1);

namespace Thrustbit\Security\Domain\User\Values;

use Thrustbit\Security\Application\Validation\Assertion\Secure;
use Thrustbit\Security\Application\Values\EncodedPassword;
use Thrustbit\Security\Application\Values\SecurityValue;

final class BcryptPassword extends Password implements EncodedPassword
{
    public function __construct($password)
    {
        parent::__construct($password);
    }

    protected function validate($password): void
    {
        Secure::length($password, 60, 'Invalid credential');
    }

    public function sameValueAs(SecurityValue $aValue): bool
    {
        return $aValue instanceof $this && $this->getCredential() === $aValue->getCredential();
    }
}