<?php

declare(strict_types=1);

namespace Thrustbit\Security\Domain\User\Values;

use Ramsey\Uuid\Uuid;
use Thrustbit\Security\Application\Values\SecurityValue;
use Thrustbit\Security\Application\Values\Uid;
use Thrustbit\Security\Application\Values\Uuid as UniqueId;

class UserId extends UniqueId
{
    public static function nextIdentity(): Uid
    {
        return new self(Uuid::uuid4());
    }

    public static function fromString($uid): Uid
    {
        static::$message = 'User id is not valid';

        static::validate($uid);

        return new self(Uuid::fromString($uid));
    }

    public function sameValueAs(SecurityValue $aValue): bool
    {
        return $aValue instanceof $this
            && $this->uuid->equals($aValue->getUid());
    }
}