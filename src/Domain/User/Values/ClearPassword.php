<?php

declare(strict_types=1);

namespace Thrustbit\Security\Domain\User\Values;

use Thrustbit\Security\Application\Validation\Assertion\Secure;
use Thrustbit\Security\Application\Values\SecurityValue;

class ClearPassword extends Password
{
    protected function validate($password): void
    {
        Secure::string($password, 'Password is invalid.');

        $message = sprintf('Password must be between %s and %s', self::MIN_LENGTH, self::MAX_LENGTH);

        Secure::betweenLength($password, self::MIN_LENGTH, self::MAX_LENGTH, $message);
    }

    public function sameValueAs(SecurityValue $aValue): bool
    {
        return $aValue instanceof $this && $this->getCredential() === $aValue->getCredential();
    }
}