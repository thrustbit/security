<?php

declare(strict_types=1);

namespace Thrustbit\Security\Domain\User\Values;

use Thrustbit\Security\Application\Values\Credential;

abstract class Password implements Credential
{
    const MIN_LENGTH = 8;
    const MAX_LENGTH = 255;

    /**
     * @var string
     */
    protected $password;

    public function __construct($password)
    {
        if (is_string($password)) {
            $password = trim($password);
        }

        $this->validate($password);

        $this->password = $password;
    }

    abstract protected function validate($password): void;

    public function getCredential(): string
    {
        return $this->password;
    }

    public function __toString(): string
    {
        return $this->password;
    }
}