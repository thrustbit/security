<?php

declare(strict_types=1);

namespace Thrustbit\Security\Domain\User\Values;

use Thrustbit\Security\Application\Validation\Assertion\Secure;
use Thrustbit\Security\Application\Values\EmailAddress as SecurityEmail;
use Thrustbit\Security\Application\Values\SecurityValue;

class EmailAddress implements SecurityEmail
{
    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected static $message = 'Email address provided is not valid.';

    protected function __construct(string $email)
    {
        $this->email = $email;
    }

    public static function fromString($email): self
    {
        Secure::string($email, static::$message);
        Secure::email($email, static::$message);

        return new self($email);
    }

    public function sameValueAs(SecurityValue $aValue): bool
    {
        return $aValue instanceof $this
            && $this->email === $aValue->getEmail();
    }

    public function read(): string
    {
        return $this->email;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function __toString(): string
    {
        return $this->email;
    }
}