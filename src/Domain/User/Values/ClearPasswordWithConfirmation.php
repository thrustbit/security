<?php

declare(strict_types=1);

namespace Thrustbit\Security\Domain\User\Values;

use Thrustbit\Security\Application\Validation\Assertion\Secure;

final class ClearPasswordWithConfirmation extends ClearPassword
{
    public function __construct($password, $passwordConfirmation)
    {
        parent::__construct($password);

        Secure::same($password, $passwordConfirmation, 'Password confirmation does not match');
    }
}