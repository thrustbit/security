<?php

declare(strict_types=1);

namespace Thrustbit\Security\Domain\User\Values;

use Thrustbit\Security\Application\Validation\Assertion\Secure;
use Thrustbit\Security\Application\Values\Name;
use Thrustbit\Security\Application\Values\SecurityIdentifier;
use Thrustbit\Security\Application\Values\SecurityValue;

class UserName implements Name, SecurityIdentifier
{
    const MIN_LENGTH = 6;
    const MAX_LENGTH = 150;

    /**
     * @var string
     */
    private $userName;

    private function __construct(string $userName)
    {
        $this->userName = $userName;
    }

    public static function fromString($userName): self
    {
        Secure::string($userName, 'User name is not valid');
        Secure::betweenLength($userName, self::MIN_LENGTH, self::MAX_LENGTH,
            sprintf('User name must be between %s and %s characters', self::MIN_LENGTH, self::MAX_LENGTH)
        );

        return new self($userName);
    }

    public function getUserName(): string
    {
        return $this->userName;
    }

    public function sameValueAs(SecurityValue $aValue): bool
    {
        return $aValue instanceof $this && $this->userName === $aValue->getUserName();
    }

    public function read(): string
    {
        return $this->userName;
    }

    public function __toString(): string
    {
        return $this->getUserName();
    }
}