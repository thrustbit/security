<?php

declare(strict_types=1);

namespace Thrustbit\Security\Domain\User\Services;

use Thrustbit\Security\Domain\User\UserSecurity;

interface SecurityUserChecker
{
    public function onPreAuthentication(UserSecurity $user): void;

    public function onPostAuthentication(UserSecurity $user): void;
}