<?php

declare(strict_types=1);

namespace Thrustbit\Security\Domain\User\Provider;

use Thrustbit\Security\Application\Values\SecurityIdentifier;
use Thrustbit\Security\Domain\User\UserSecurity;

interface UserProvider
{
    public function requireByIdentifier(SecurityIdentifier $identifier): UserSecurity;

    public function refreshUser(UserSecurity $user): UserSecurity;

    public function supportsClass(string $class): bool;
}