<?php

declare(strict_types=1);

namespace Thrustbit\Security\Domain\User;

interface UserThrottleExtended extends UserThrottle
{
    public function isActivated(): bool;

    public function isSuspended(): bool;

    public function isBanned(): bool;

    public function isCredentialsNonExpired(): bool;
}