<?php

declare(strict_types=1);

namespace Thrustbit\Security\Domain\User;

use Thrustbit\Security\Application\Values\EncodedPassword;

interface LocalUser extends UserSecurity
{
    public function getPassword(): EncodedPassword;
}