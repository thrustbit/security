<?php

declare(strict_types=1);

namespace Thrustbit\Security\Domain\User;

interface UserThrottle
{
    public function isEnabled(): bool;

    public function isNonLocked(): bool;
}