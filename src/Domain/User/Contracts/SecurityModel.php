<?php

declare(strict_types=1);

namespace Thrustbit\Security\Domain\User\Contracts;

use Thrustbit\Security\Domain\User\UserSecurity;

interface SecurityModel
{
    public function serializableUserSecurity(): UserSecurity;
}