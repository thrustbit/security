<?php

declare(strict_types=1);

namespace Thrustbit\Security\Domain\User;

use Thrustbit\Security\Application\Values\EmailAddress;
use Thrustbit\Security\Application\Values\Name;
use Thrustbit\Security\Application\Values\SecurityIdentifier;
use Thrustbit\Security\Application\Values\Uid;

interface UserSecurity
{
    /**
     * @return SecurityIdentifier|Uid
     */
    public function getId(): SecurityIdentifier;

    public function getIdentifier(): SecurityIdentifier;

    public function getEmail(): EmailAddress;

    public function getUserName(): Name;

    public function getRoles(): array;
}