<?php

declare(strict_types=1);

namespace Thrustbit\Security\Domain\User\Exceptions;

use Thrustbit\Security\Application\Exceptions\AuthenticationException;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Values\EmptyCredential;

class BadCredentials extends AuthenticationException
{
    public static function hasChanged(): BadCredentials
    {
        return new static('Credentials were changed from another session');
    }

    public static function invalid($credentials = null): BadCredentials
    {
        $message = 'Invalid credentials';

        if ($credentials instanceof EmptyCredential) {
            $message = 'Credentials can not be empty';
        }

        return new static($message);
    }
}