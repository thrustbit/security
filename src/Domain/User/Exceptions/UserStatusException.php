<?php

declare(strict_types=1);

namespace Thrustbit\Security\Domain\User\Exceptions;

use Thrustbit\Security\Application\Exceptions\AuthenticationException;

class UserStatusException extends AuthenticationException
{
}