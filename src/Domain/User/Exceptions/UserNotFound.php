<?php

declare(strict_types=1);

namespace Thrustbit\Security\Domain\User\Exceptions;

use Thrustbit\Security\Application\Exceptions\AuthenticationException;

class UserNotFound extends AuthenticationException
{
    public static function withException(AuthenticationException $exception): UserNotFound
    {
        return new static('User not found', 0, $exception);
    }
}