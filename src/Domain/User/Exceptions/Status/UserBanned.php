<?php

declare(strict_types=1);

namespace Thrustbit\Security\Domain\User\Exceptions\Status;

class UserBanned extends UserLocked
{
}