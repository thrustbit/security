<?php

declare(strict_types=1);

namespace Thrustbit\Security\Domain\User\Exceptions\Status;

use Thrustbit\Security\Domain\User\Exceptions\UserStatusException;

class UserNotEnabled extends UserStatusException
{
    public static function withException(UserStatusException $exception): UserNotEnabled
    {
        return new self($exception->getMessage());
    }
}