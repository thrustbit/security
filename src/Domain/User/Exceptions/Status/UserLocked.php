<?php

declare(strict_types=1);

namespace Thrustbit\Security\Domain\User\Exceptions\Status;

use Thrustbit\Security\Domain\User\Exceptions\UserStatusException;

class UserLocked extends UserStatusException
{
}