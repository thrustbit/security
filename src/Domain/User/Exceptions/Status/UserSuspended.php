<?php

declare(strict_types=1);

namespace Thrustbit\Security\Domain\User\Exceptions\Status;

class UserSuspended extends UserLocked
{
}