<?php

declare(strict_types=1);

namespace Thrustbit\Security\Domain\User\Generic;

use Thrustbit\Security\Application\Values\EmailAddress;
use Thrustbit\Security\Application\Values\Name;
use Thrustbit\Security\Application\Values\SecurityIdentifier;
use Thrustbit\Security\Domain\User\Values\EmailAddress as Email;
use Thrustbit\Security\Domain\User\Values\UserId;
use Thrustbit\Security\Domain\User\Values\UserName;

class GenericUser extends GenericAccount
{
    public function getId(): SecurityIdentifier
    {
        return UserId::fromString($this->attributes['id']);
    }

    public function getIdentifier(): SecurityIdentifier
    {
        return $this->getId();
    }

    public function getEmail(): EmailAddress
    {
        return Email::fromString($this->attributes['email']);
    }

    public function getUserName(): Name
    {
        return UserName::fromString($this->attributes['user_name']);
    }
}