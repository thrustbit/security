<?php

declare(strict_types=1);

namespace Thrustbit\Security\Domain\User\Generic;

use Thrustbit\Security\Application\Values\EncodedPassword;
use Thrustbit\Security\Domain\User\LocalUser;
use Thrustbit\Security\Domain\User\Values\BcryptPassword;

class GenericLocalUser extends GenericUser implements LocalUser
{
    public function getPassword(): EncodedPassword
    {
        return new BcryptPassword($this->attributes['password']);
    }
}