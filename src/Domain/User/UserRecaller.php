<?php

declare(strict_types=1);

namespace Thrustbit\Security\Domain\User;

interface UserRecaller
{
    public function getRecallerToken(): ?string;

    /**
     * @param string $token
     *
     * @return null|UserSecurity
     */
    public function updateRecallerToken(string $token);
}