<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Http\Request;
use Thrustbit\Security\Application\Http\Request\Event\UserImpersonated;
use Thrustbit\Security\Application\Http\Request\Event\UserLogin;
use Thrustbit\Security\Application\Http\Request\Event\UserLogout;
use Thrustbit\Security\Domain\User\UserSecurity;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;
use Thrustbit\Security\Infrastructure\Guard\Contracts\EventGuard;

class SecurityEventDispatcher implements EventGuard
{
    /**
     * @var Dispatcher
     */
    private $eventDispatcher;

    public function __construct(Dispatcher $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    public function dispatch($event): void
    {
        $this->eventDispatcher->dispatch($event);
    }

    public function dispatchLoginEvent(Request $request, Tokenable $token): void
    {
        $this->eventDispatcher->dispatch(new UserLogin($request, $token));
    }

    public function dispatchLogoutEvent(Tokenable $token): void
    {
        $this->eventDispatcher->dispatch(new UserLogout($token));
    }

    public function dispatchImpersonateUserEvent(Request $request, UserSecurity $user): void
    {
        $this->eventDispatcher->dispatch(new UserImpersonated($request, $user));
    }
}