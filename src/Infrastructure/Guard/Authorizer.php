<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard;

use Thrustbit\Security\Application\Exceptions\AuthorizationException;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;
use Thrustbit\Security\Infrastructure\Guard\Authorization\Contract\Authorizable;

class Authorizer
{
    /**
     * @var Authorizable
     */
    private $authorize;

    public function __construct(Authorizable $authorize)
    {
        $this->authorize = $authorize;
    }

    public function grant(Tokenable $token, array $attributes, $object = null): bool
    {
        return $this->authorize->decide($token, $attributes, $object);
    }

    public function requireGranted(Tokenable $token, array $attributes = [], $object = null): bool
    {
        if ($this->grant($token, $attributes, $object)) {
            return true;
        }

        throw new AuthorizationException('Authorization denied');
    }
}