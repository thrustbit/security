<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard;

use Thrustbit\Security\Application\Exceptions\Service\CredentialsNotFound;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Storage;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;
use Thrustbit\Security\Infrastructure\Guard\Contracts\StorageGuard as StorageGuardContract;

class StorageGuard implements StorageGuardContract
{
    /**
     * @var Storage
     */
    private $storage;

    public function __construct(Storage $storage)
    {
        $this->storage = $storage;
    }

    public function put(Tokenable $token): void
    {
        $this->storage->setToken($token);
    }

    public function get(): ?Tokenable
    {
        return $this->storage->getToken();
    }

    public function required(): Tokenable
    {
        if ($token = $this->storage->getToken()) {
            return $token;
        }

        throw new CredentialsNotFound('No token in storage');
    }

    public function clear(): void
    {
        $this->storage->setToken(null);
    }

    public function isEmpty(): bool
    {
        return null === $this->storage->getToken();
    }

    public function isNotEmpty(): bool
    {
        return !$this->isEmpty();
    }
}