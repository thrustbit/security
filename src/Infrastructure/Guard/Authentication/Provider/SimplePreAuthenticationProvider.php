<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authentication\Provider;

use Thrustbit\Security\Application\Values\SecurityKey\ProviderKey;
use Thrustbit\Security\Domain\User\Provider\UserProvider;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\AuthenticationProvider;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\SimplePreAuthenticator;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

class SimplePreAuthenticationProvider implements AuthenticationProvider
{
    /**
     * @var SimplePreAuthenticator
     */
    private $authenticator;

    /**
     * @var UserProvider
     */
    private $userProvider;

    /**
     * @var ProviderKey
     */
    private $providerKey;

    public function __construct(SimplePreAuthenticator $authenticator,
                                UserProvider $userProvider,
                                ProviderKey $providerKey)
    {
        $this->authenticator = $authenticator;
        $this->userProvider = $userProvider;
        $this->providerKey = $providerKey;
    }

    public function authenticate(Tokenable $token): Tokenable
    {
        return $this->authenticator->authenticateToken(
            $token, $this->userProvider, $this->providerKey
        );
    }

    public function supports(Tokenable $token): bool
    {
        return $this->authenticator->supportsToken($token, $this->providerKey);
    }
}