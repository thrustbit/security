<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authentication\Provider;

use Thrustbit\Security\Application\Exceptions\Service\UnsupportedProvider;
use Thrustbit\Security\Application\Values\SecurityKey\ProviderKey;
use Thrustbit\Security\Application\Values\SecurityKey\RecallerKey;
use Thrustbit\Security\Domain\User\Exceptions\BadCredentials;
use Thrustbit\Security\Domain\User\Services\SecurityUserChecker;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\AuthenticationProvider;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\RecallerToken;

class RecallerAuthenticationProvider implements AuthenticationProvider
{
    /**
     * @var ProviderKey
     */
    private $providerKey;

    /**
     * @var RecallerKey
     */
    private $recallerKey;

    /**
     * @var SecurityUserChecker
     */
    private $userChecker;

    public function __construct(ProviderKey $providerKey, RecallerKey $recallerKey, SecurityUserChecker $userChecker)
    {
        $this->providerKey = $providerKey;
        $this->recallerKey = $recallerKey;
        $this->userChecker = $userChecker;
    }

    public function authenticate(Tokenable $token): Tokenable
    {
        if (!$this->supports($token)) {
            throw UnsupportedProvider::withSupport($this, $token);
        }

        if (!$this->recallerKey->sameValueAs($token->getRecallerKey())) {
            throw new BadCredentials('Invalid credentials');
        }

        $this->userChecker->onPreAuthentication(
            $user = $token->getUser()
        );

        return new RecallerToken($user, $this->providerKey, $this->recallerKey);
    }

    public function supports(Tokenable $token): bool
    {
        return $token instanceof RecallerToken
            && $this->providerKey->sameValueAs($token->getSecurityKey());
    }
}