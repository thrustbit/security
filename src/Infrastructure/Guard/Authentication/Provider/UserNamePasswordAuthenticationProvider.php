<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authentication\Provider;

use Illuminate\Contracts\Hashing\Hasher;
use Thrustbit\Security\Application\Exceptions\Service\UnsupportedProvider;
use Thrustbit\Security\Application\Values\SecurityKey\ProviderKey;
use Thrustbit\Security\Domain\Role\Values\SwitchUserRole;
use Thrustbit\Security\Domain\User\Exceptions\BadCredentials;
use Thrustbit\Security\Domain\User\Exceptions\UserNotFound;
use Thrustbit\Security\Domain\User\LocalUser;
use Thrustbit\Security\Domain\User\Provider\UserProvider;
use Thrustbit\Security\Domain\User\Services\SecurityUserChecker;
use Thrustbit\Security\Domain\User\UserSecurity;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\AuthenticationProvider;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\UserNamePasswordToken;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Values\EmptyCredential;

class UserNamePasswordAuthenticationProvider implements AuthenticationProvider
{
    /**
     * @var ProviderKey
     */
    private $providerKey;

    /**
     * @var UserProvider
     */
    private $userProvider;

    /**
     * @var SecurityUserChecker
     */
    private $userChecker;

    /**
     * @var Hasher
     */
    private $encoder;

    /**
     * @var bool
     */
    private $hideException;

    public function __construct(ProviderKey $providerKey,
                                UserProvider $userProvider,
                                SecurityUserChecker $userChecker,
                                Hasher $encoder,
                                bool $hideException = true)
    {
        $this->providerKey = $providerKey;
        $this->userProvider = $userProvider;
        $this->userChecker = $userChecker;
        $this->encoder = $encoder;
        $this->hideException = $hideException;
    }

    public function authenticate(Tokenable $token): Tokenable
    {
        if (!$this->supports($token)) {
            throw UnsupportedProvider::withSupport($this, $token);
        }

        try {
            $user = $this->retrieveUser($token);

            $this->checkUser($user, $token);
        } catch (BadCredentials $badCredentials) {
            if ($this->hideException) {
                throw UserNotFound::withException($badCredentials);
            }

            throw $badCredentials;
        }

        return $this->createAuthenticatedToken($user, $token);
    }

    private function retrieveUser(Tokenable $token): LocalUser
    {
        if ($token->getUser() instanceof LocalUser) {
            return $token->getUser();
        }

        return $this->userProvider->requireByIdentifier($token->getIdentifier());
    }

    private function checkUser(LocalUser $user, Tokenable $token): void
    {
        $this->userChecker->onPreAuthentication($user);

        $this->checkCredentials($user, $token);

        $this->userChecker->onPostAuthentication($user);
    }

    private function createAuthenticatedToken(LocalUser $user, Tokenable $token): UserNamePasswordToken
    {
        return new UserNamePasswordToken(
            $user,
            $token->getCredential(),
            $this->providerKey,
            $this->getRoles($user, $token)
        );
    }

    private function getRoles(UserSecurity $user, Tokenable $token): array
    {
        $roles = $user->getRoles();

        foreach ($token->getRoles() as $role) {
            if ($role instanceof SwitchUserRole) {
                $roles[] = $role;
                break;
            }
        }

        return $roles;
    }

    private function checkCredentials(LocalUser $user, Tokenable $token): void
    {
        $currentUser = $token->getUser();

        if ($currentUser instanceof LocalUser) {
            if (!$currentUser->getPassword()->sameValueAs($user->getPassword())) {
                throw BadCredentials::hasChanged();
            }
        } else {
            $presentedPassword = $token->getCredential();

            if ($presentedPassword instanceof EmptyCredential) {
                throw BadCredentials::invalid($presentedPassword);
            }

            if (!$this->encoder->check($presentedPassword->getCredential(), $user->getPassword()->getCredential())) {
                throw BadCredentials::invalid();
            }
        }
    }

    public function supports(Tokenable $token): bool
    {
        return $token instanceof UserNamePasswordToken
            && $token->getSecurityKey()->sameValueAs($this->providerKey);
    }
}