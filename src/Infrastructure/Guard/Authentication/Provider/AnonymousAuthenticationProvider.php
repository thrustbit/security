<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authentication\Provider;

use Thrustbit\Security\Application\Exceptions\Service\UnsupportedProvider;
use Thrustbit\Security\Application\Values\SecurityKey\AnonymousKey;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\AuthenticationProvider;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\AnonymousToken;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

class AnonymousAuthenticationProvider implements AuthenticationProvider
{
    /**
     * @var AnonymousKey
     */
    private $anonymousKey;

    public function __construct(AnonymousKey $anonymousKey)
    {
        $this->anonymousKey = $anonymousKey;
    }

    public function authenticate(Tokenable $token): Tokenable
    {
        if (!$this->supports($token)) {
            throw UnsupportedProvider::withSupport($this, $token);
        }

        return $token;
    }

    public function supports(Tokenable $token): bool
    {
        return $token instanceof AnonymousToken
            && $this->anonymousKey->sameValueAs($token->getSecurityKey());
    }
}