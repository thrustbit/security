<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authentication;

use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\TrustResolver;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

class DefaultTrustResolver implements TrustResolver
{
    /**
     * @var string
     */
    private $anonymousClass;

    /**
     * @var string
     */
    private $recallerClass;

    public function __construct(string $anonymousClass, string $recallerClass)
    {
        $this->anonymousClass = $anonymousClass;
        $this->recallerClass = $recallerClass;
    }

    public function isFullFledged(Tokenable $token = null): bool
    {
        if (!$token) {
            return false;
        }

        return !$this->isAnonymous($token) && !$this->isRememberMe($token);
    }

    public function isAnonymous(Tokenable $token = null): bool
    {
        if (!$token) {
            return false;
        }

        return $token instanceof $this->anonymousClass;
    }

    public function isRememberMe(Tokenable $token = null): bool
    {
        if (!$token) {
            return false;
        }

        return $token instanceof $this->recallerClass;
    }
}