<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authentication\Contract;

use Thrustbit\Security\Application\Values\SecurityKey;
use Thrustbit\Security\Domain\User\Provider\UserProvider;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

interface SimpleAuthenticator
{
    public function authenticateToken(Tokenable $token, UserProvider $userProvider, SecurityKey $securityKey): Tokenable;

    public function supportsToken(Tokenable $token, SecurityKey $securityKey): bool;
}