<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authentication\Contract;

use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

interface AuthenticationProvider extends Authenticatable
{
    public function supports(Tokenable $token): bool;
}