<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authentication\Contract;

use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

interface Authenticatable
{
    public function authenticate(Tokenable $token): Tokenable;
}