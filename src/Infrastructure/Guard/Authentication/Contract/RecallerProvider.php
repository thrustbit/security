<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authentication\Contract;

use Thrustbit\Security\Application\Values\SecurityIdentifier;
use Thrustbit\Security\Domain\User\UserRecaller;
use Thrustbit\Security\Domain\User\UserSecurity;

interface RecallerProvider
{
    public function requireByRecallerIdentifier(SecurityIdentifier $identifier, string $recallerToken): UserSecurity;

    public function requireByRecallerToken(string $recallerToken): UserSecurity;

    public function refreshRecaller(UserRecaller $user, string $newRecallerToken): UserSecurity;
}