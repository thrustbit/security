<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authentication\Contract;

use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

interface TrustResolver
{
    public function isFullFledged(Tokenable $token = null): bool;

    public function isAnonymous(Tokenable $token = null): bool;

    public function isRememberMe(Tokenable $token = null): bool;
}