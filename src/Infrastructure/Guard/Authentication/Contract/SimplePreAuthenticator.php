<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authentication\Contract;

use Illuminate\Http\Request;
use Thrustbit\Security\Application\Values\SecurityKey;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

interface SimplePreAuthenticator extends SimpleAuthenticator
{
    public function createToken(Request $request, SecurityKey $securityKey): Tokenable;
}