<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authentication\Token;

use Thrustbit\Security\Application\Values\Credential;
use Thrustbit\Security\Application\Values\SecurityKey;
use Thrustbit\Security\Application\Values\SecurityKey\ProviderKey;
use Thrustbit\Security\Application\Values\SecurityKey\RecallerKey;
use Thrustbit\Security\Domain\User\UserSecurity;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Values\EmptyCredential;

class RecallerToken extends Token
{
    /**
     * @var ProviderKey
     */
    private $providerKey;

    /**
     * @var RecallerKey
     */
    private $recallerKey;

    public function __construct(UserSecurity $user, ProviderKey $providerKey, RecallerKey $recallerKey)
    {
        parent::__construct($user->getRoles(), count($user->getRoles()) > 0);

        $this->setUser($user);
        $this->providerKey = $providerKey;
        $this->recallerKey = $recallerKey;
    }

    public function getCredential(): Credential
    {
        return new EmptyCredential;
    }

    public function getSecurityKey(): SecurityKey
    {
        return $this->providerKey;
    }

    public function getRecallerKey(): RecallerKey
    {
        return $this->recallerKey;
    }

    public function serialize(): string
    {
        return serialize([$this->recallerKey, $this->providerKey, parent::serialize()]);
    }

    public function unserialize($serialized): void
    {
        [$this->recallerKey, $this->providerKey, $parentStr] = unserialize($serialized, [RecallerToken::class]);

        parent::unserialize($parentStr);
    }
}