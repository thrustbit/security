<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Storage;

use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Storage;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

class TokenStorage implements Storage
{
    /**
     * @var Tokenable|null
     */
    private $token;

    public function getToken(): ?Tokenable
    {
        return $this->token;
    }

    public function setToken(Tokenable $token = null): void
    {
        $this->token = $token;
    }
}