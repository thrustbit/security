<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authentication\Token;

use Thrustbit\Security\Application\Values\Credential;
use Thrustbit\Security\Application\Values\SecurityKey;
use Thrustbit\Security\Application\Values\SecurityKey\AnonymousKey;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Values\AnonymousIdentifier;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Values\EmptyCredential;

class AnonymousToken extends Token
{
    /**
     * @var AnonymousKey
     */
    private $key;

    public function __construct(AnonymousIdentifier $identifier, AnonymousKey $key, array $roles = [])
    {
        parent::__construct($roles, true);

        $this->setUser($identifier);
        $this->key = $key;
    }

    public function getCredential(): Credential
    {
        return new EmptyCredential;
    }

    public function getSecurityKey(): SecurityKey
    {
        return $this->key;
    }
}