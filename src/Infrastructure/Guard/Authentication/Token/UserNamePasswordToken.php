<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authentication\Token;

use Thrustbit\Security\Application\Values\Credential;
use Thrustbit\Security\Application\Values\SecurityIdentifier;
use Thrustbit\Security\Application\Values\SecurityKey;
use Thrustbit\Security\Application\Values\SecurityKey\ProviderKey;
use Thrustbit\Security\Domain\User\UserSecurity;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Values\EmptyCredential;

class UserNamePasswordToken extends Token
{
    /**
     * @var Credential
     */
    protected $credential;

    /**
     * @var ProviderKey
     */
    protected $securityKey;

    /**
     * UserNamePasswordToken constructor.
     *
     * @param SecurityIdentifier|UserSecurity $user
     * @param Credential $credential
     * @param ProviderKey $providerKey
     * @param array $roles
     */
    public function __construct($user, Credential $credential, ProviderKey $providerKey, array $roles = [])
    {
        parent::__construct($roles, (count($roles) > 0));

        $this->setUser($user);

        $this->credential = $credential;
        $this->securityKey = $providerKey;
    }

    public function eraseCredentials(): void
    {
        $this->credential = new EmptyCredential();

        parent::eraseCredentials();
    }

    public function getCredential(): Credential
    {
        return $this->credential;
    }

    public function getSecurityKey(): SecurityKey
    {
        return $this->securityKey;
    }

    public function serialize(): string
    {
        return serialize([$this->credential, $this->securityKey, parent::serialize()]);
    }

    public function unserialize($serialized)
    {
        [$this->credential, $this->securityKey, $parentStr] = unserialize($serialized, [Tokenable::class]);

        parent::unserialize($parentStr);
    }
}