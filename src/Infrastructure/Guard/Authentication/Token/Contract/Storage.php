<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract;

interface Storage
{
    public function getToken(): ?Tokenable;

    public function setToken(Tokenable $token = null): void;
}