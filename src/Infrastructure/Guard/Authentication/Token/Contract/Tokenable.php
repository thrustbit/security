<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract;

use Thrustbit\Security\Application\Values\Credential;
use Thrustbit\Security\Application\Values\SecurityIdentifier;
use Thrustbit\Security\Application\Values\SecurityKey;

interface Tokenable
{
    public function getRoles(): array;

    public function setUser($user): void;

    public function getUser();

    public function getIdentifier(): SecurityIdentifier;

    public function isAuthenticated(): bool;

    public function setAuthenticated(bool $authenticated): void;

    public function getCredential(): Credential;

    public function eraseCredentials(): void;

    public function getSecurityKey(): SecurityKey;

    public function __toString(): string;
}