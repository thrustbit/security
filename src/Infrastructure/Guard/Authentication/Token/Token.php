<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authentication\Token;

use Thrustbit\Security\Application\Exceptions\Service\InvalidArgument;
use Thrustbit\Security\Application\Values\SecurityIdentifier;
use Thrustbit\Security\Domain\Role\RoleSecurity as RoleContract;
use Thrustbit\Security\Domain\Role\Values\RoleValue;
use Thrustbit\Security\Domain\User\UserSecurity;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Concerns\HasAttributes;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Concerns\HasChanged;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Concerns\HasSerializer;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

abstract class Token implements Tokenable, \Serializable
{
    use HasAttributes, HasChanged, HasSerializer;

    /**
     * @var array
     */
    private $roles;

    /**
     * @var UserSecurity|SecurityIdentifier
     */
    private $user;

    /**
     * @var bool
     */
    private $authenticated = false;

    public function __construct(array $roles = [], bool $authenticated = false)
    {
        foreach ($roles as &$role) {
            if (is_string($role)) {
                $role = new RoleValue($role);
            }

            if (!$role instanceof RoleContract) {
                throw InvalidArgument::reason(
                    sprintf('Role must be a string or implement %s', RoleContract::class)
                );
            }
        }

        $this->roles = $roles;
        $this->authenticated = $authenticated;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function setUser($user): void
    {
        $this->guardUser($user);

        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getIdentifier(): SecurityIdentifier
    {
        if ($this->user instanceof SecurityIdentifier) {
            return $this->user;
        }

        return $this->user->getIdentifier();
    }

    public function isAuthenticated(): bool
    {
        return $this->authenticated;
    }

    public function setAuthenticated(bool $authenticated): void
    {
        $this->authenticated = $authenticated;
    }

    public function eraseCredentials(): void
    {
        // override
    }
}