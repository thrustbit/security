<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Concerns;

trait HasAttributes
{
    /**
     * @var array
     */
    private $attributes = [];

    public function setAttribute(string $attribute, $value): void
    {
        $this->attributes[$attribute] = $value;
    }

    /**
     * @param string $attribute
     * @param null $default
     *
     * @return mixed
     */
    public function getAttribute(string $attribute, $default = null)
    {
        if ($this->hasAttribute($attribute)) {
            return $this->attributes[$attribute];
        }

        return $default;
    }

    public function hasAttribute(string $attribute): bool
    {
        return isset($this->attributes[$attribute]);
    }

    public function forgetAttribute(string $attribute): bool
    {
        if ($this->hasAttribute($attribute)) {
            $this->__unset($attribute);

            return true;
        }

        return false;
    }

    public function getAttributes(): array
    {
        return $this->attributes;
    }

    public function __get(string $key)
    {
        return $this->attributes[$key];
    }

    public function __set(string $key, $value): void
    {
        $this->attributes[$key] = $value;
    }

    public function __isset(string $key): bool
    {
        return isset($this->attributes[$key]);
    }

    public function __unset(string $key): void
    {
        unset($this->attributes[$key]);
    }
}