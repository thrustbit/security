<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Concerns;

use Thrustbit\Security\Domain\User\Contracts\SecurityModel;
use Thrustbit\Security\Domain\User\UserSecurity;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

trait HasSerializer
{
    public function serialize(): string
    {
        return serialize(
            [
                $this->serializeUserSecurity(),
                $this->authenticated,
                array_map(function ($role) { return clone $role;}, $this->roles),
                $this->attributes
            ]
        );
    }

    public function unserialize($serialized)
    {
        [
            $this->user,
            $this->authenticated,
            $this->roles,
            $this->attributes
        ] = unserialize($serialized, [Tokenable::class]);
    }

    public function __toString(): string
    {
        return json_encode($this->user);
    }

    protected function serializeUserSecurity(): UserSecurity
    {
        if ($this->getUser() instanceof SecurityModel) {
            return $this->getUser()->serializableUserSecurity();
        }

        return clone $this->getUser();
    }
}