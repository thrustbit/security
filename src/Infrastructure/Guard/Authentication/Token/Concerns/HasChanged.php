<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Concerns;

use Thrustbit\Security\Application\Exceptions\Service\InvalidArgument;
use Thrustbit\Security\Application\Values\SecurityIdentifier;
use Thrustbit\Security\Domain\User\UserSecurity;

trait HasChanged
{
    private function guardUser($user): void
    {
        if (!$user instanceof UserSecurity && !$user instanceof SecurityIdentifier) {
            throw InvalidArgument::reason(
                sprintf('User must implement %s or %s',
                    UserSecurity::class, SecurityIdentifier::class)
            );
        }
    }

    private function hasChanged(UserSecurity $user): bool
    {
        // todo

        return false;
    }
}