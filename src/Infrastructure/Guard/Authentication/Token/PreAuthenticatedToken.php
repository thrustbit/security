<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authentication\Token;

use Thrustbit\Security\Application\Values\Credential;
use Thrustbit\Security\Application\Values\SecurityIdentifier;
use Thrustbit\Security\Application\Values\SecurityKey;
use Thrustbit\Security\Application\Values\SecurityKey\ProviderKey;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Values\EmptyCredential;

class PreAuthenticatedToken extends Token
{
    /**
     * @var Credential
     */
    private $credential;

    /**
     * @var ProviderKey
     */
    private $providerKey;

    public function __construct(SecurityIdentifier $identifier, Credential $credential, ProviderKey $providerKey, array $roles = [])
    {
        parent::__construct($roles, count($roles)> 0);

        $this->setUser($identifier);
        $this->credential = $credential;
        $this->providerKey = $providerKey;
    }

    public function getCredential(): Credential
    {
        return $this->credential;
    }

    public function getSecurityKey(): SecurityKey
    {
        return $this->providerKey;
    }

    public function eraseCredentials(): void
    {
        parent::eraseCredentials();

        $this->credential = new EmptyCredential();
    }

    public function serialize(): string
    {
        return serialize([$this->credential, $this->providerKey, parent::serialize()]);
    }

    public function unserialize($string): void
    {
        [$this->credential, $this->providerKey, $parentStr] = unserialize($string, [PreAuthenticatedToken::class]);

        parent::unserialize($parentStr);
    }
}