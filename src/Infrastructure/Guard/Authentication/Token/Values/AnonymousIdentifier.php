<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Values;

use Thrustbit\Security\Application\Values\SecurityIdentifier;
use Thrustbit\Security\Application\Values\SecurityValue;

final class AnonymousIdentifier implements SecurityIdentifier
{
    const IDENTIFIER = '.anon';

    public function read(): string
    {
        return static::IDENTIFIER;
    }

    public function sameValueAs(SecurityValue $aValue): bool
    {
        return $aValue instanceof $this && static::IDENTIFIER === $aValue->read();
    }

    public function __toString(): string
    {
        return $this->read();
    }
}