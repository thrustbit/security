<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Values;

use Thrustbit\Security\Application\Values\Credential;
use Thrustbit\Security\Domain\User\Values\ClearPassword;

final class SimpleCredentialFactory
{
    public static function fromString($credentials): Credential
    {
        if (is_string($credentials)) {
            $credentials = trim($credentials);
        }

        if (null === $credentials || (is_string($credentials) && empty($credentials))) {
            return new EmptyCredential;
        }

        return new ClearPassword($credentials);
    }

    private function __construct()
    {
    }
}