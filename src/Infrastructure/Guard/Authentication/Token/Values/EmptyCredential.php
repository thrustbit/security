<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Values;

use Thrustbit\Security\Application\Values\Credential;
use Thrustbit\Security\Application\Values\SecurityValue;

final class EmptyCredential implements Credential
{
    public function getCredential(): string
    {
        return '';
    }

    public function sameValueAs(SecurityValue $aValue): bool
    {
        return $aValue instanceof $this;
    }

    public function __toString(): string
    {
        return $this->getCredential();
    }
}