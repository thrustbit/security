<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Values;

use Thrustbit\Security\Application\Values\SecurityIdentifier;
use Thrustbit\Security\Application\Values\SecurityValue;

final class NilTokenIdentifier implements SecurityIdentifier
{
    public function read()
    {
        return null;
    }

    public function sameValueAs(SecurityValue $aValue): bool
    {
        return $aValue instanceof $this;
    }
}