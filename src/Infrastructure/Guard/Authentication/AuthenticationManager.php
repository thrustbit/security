<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authentication;

use Thrustbit\Security\Application\Exceptions\Service\UnsupportedProvider;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\Authenticatable;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\AuthenticationProvider;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

class AuthenticationManager implements Authenticatable
{
    /**
     * @var array
     */
    private $authenticationProviders;

    public function __construct(array $authenticationProviders = [])
    {
        $this->authenticationProviders = $authenticationProviders;
    }

    public function authenticate(Tokenable $token): Tokenable
    {
        $result = false;

        /** @var AuthenticationProvider $provider */
        foreach ($this->authenticationProviders as $provider) {
            if (!$provider->supports($token)) {
                continue;
            }

            $result = $provider->authenticate($token);

            if ($result) {
                break;
            }
        }

        if (!$result) {
            throw UnsupportedProvider::withSupport($this, $token);
        }

        return $result;
    }

    public function addProvider(AuthenticationProvider $provider): void
    {
        $this->authenticationProviders[] = $provider;
    }
}