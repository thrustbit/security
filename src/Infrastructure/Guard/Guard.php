<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard;

use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\Authenticatable;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Storage;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;
use Thrustbit\Security\Infrastructure\Guard\Contracts\EventGuard;
use Thrustbit\Security\Infrastructure\Guard\Contracts\Guard as GuardContract;
use Thrustbit\Security\Infrastructure\Guard\Contracts\StorageGuard;

class Guard implements GuardContract
{
    /**
     * @var Storage
     */
    private $storage;

    /**
     * @var Authenticatable
     */
    private $authentication;

    /**
     * @var SecurityEventDispatcher
     */
    private $securityDispatcher;

    public function __construct(StorageGuard $storage,
                                Authenticatable $authentication,
                                SecurityEventDispatcher $securityDispatcher)
    {
        $this->storage = $storage;
        $this->authentication = $authentication;
        $this->securityDispatcher = $securityDispatcher;
    }

    public function authenticate(Tokenable $token): Tokenable
    {
        return $this->authentication->authenticate($token);
    }

    public function storage(): StorageGuard
    {
        return $this->storage;
    }

    public function event(): EventGuard
    {
        return $this->securityDispatcher;
    }
}