<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authorization\Contract;

use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

interface Votable
{
    const ACCESS_GRANTED = 1;
    const ACCESS_ABSTAIN = 0;
    const ACCESS_DENIED = -1;

    public function vote(Tokenable $token, $subject, array $attributes): int;
}