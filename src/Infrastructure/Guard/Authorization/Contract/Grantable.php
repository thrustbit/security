<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authorization\Contract;

interface Grantable
{
    public function isGranted(array $attributes = null, $object = null): bool;

    public function forceAuthentication(bool $force): Grantable;
}