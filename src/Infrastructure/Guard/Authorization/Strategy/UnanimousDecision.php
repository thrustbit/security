<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authorization\Strategy;

use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;
use Thrustbit\Security\Infrastructure\Guard\Authorization\Contract\Authorizable;
use Thrustbit\Security\Infrastructure\Guard\Authorization\Contract\Votable;

class UnanimousDecision implements Authorizable
{
    /**
     * @var array
     */
    private $voters;

    /**
     * @var bool
     */
    private $allowIfAllAbstain;

    public function __construct(array $voters, bool $allowIfAllAbstain = false)
    {
        $this->voters = $voters;
        $this->allowIfAllAbstain = $allowIfAllAbstain;
    }

    public function decide(Tokenable $token, array $attributes, $object): bool
    {
        $grant = 0;

        foreach ($attributes as $attribute) {
            foreach ($this->voters as $voter) {
                $decision = $voter->vote($token, $object, [$attribute]);

                switch ($decision) {
                    case Votable::ACCESS_GRANTED:
                        ++$grant;
                        break;
                    case Votable::ACCESS_DENIED:
                        return false;
                    default:
                        break;
                }
            }
        }

        return ($grant > 0) ? true : $this->allowIfAllAbstain;
    }
}