<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authorization;

use Thrustbit\Security\Infrastructure\Guard\Authorization\Contract\Authorizable;
use Thrustbit\Security\Infrastructure\Guard\Authorization\Contract\Grantable;
use Thrustbit\Security\Infrastructure\Guard\Contracts\Guard;

class AuthorizationChecker implements Grantable
{
    /**
     * @var Guard
     */
    private $guard;

    /**
     * @var Authorizable
     */
    private $authorizable;

    /**
     * @var bool
     */
    private $alwaysAuthenticate;

    public function __construct(Guard $guard, Authorizable $authorizable, bool $alwaysAuthenticate = false)
    {
        $this->guard = $guard;
        $this->authorizable = $authorizable;
        $this->alwaysAuthenticate = $alwaysAuthenticate;
    }

    public function isGranted(array $attributes = null, $object = null): bool
    {
        $token = $this->guard->storage()->required();

        if ($this->alwaysAuthenticate || !$token->isAuthenticated()) {
            $this->guard->storage()->put(
               $token = $this->guard->authenticate($token)
            );
        }

        return $this->authorizable->decide($token, (array)$attributes, $object);
    }

    public function forceAuthentication(bool $force): Grantable
    {
        $this->alwaysAuthenticate = $force;

        return $this;
    }
}