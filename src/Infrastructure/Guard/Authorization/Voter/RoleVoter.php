<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authorization\Voter;

use Thrustbit\Security\Domain\Role\RoleSecurity;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

class RoleVoter extends AccessVoter
{
    /**
     * @var string
     */
    private $rolePrefix;

    public function __construct(string $rolePrefix)
    {
        $this->rolePrefix = $rolePrefix;
    }

    public function vote(Tokenable $token, $subject, array $attributes): int
    {
        $vote = $this->abstain();
        $roles = $this->extractRoles($token);

        foreach ($attributes as $attribute) {
            if (0 !== strpos($attribute, $this->rolePrefix)) {
                continue;
            }

            $vote = $this->denied();

            /** @var RoleSecurity $role */
            foreach ($roles as $role) {
                if ($attribute === $role->getRole()) {
                    return $this->granted();
                }
            }
        }

        return $vote;
    }

    protected function extractRoles(Tokenable $token): array
    {
        return $token->getRoles();
    }
}