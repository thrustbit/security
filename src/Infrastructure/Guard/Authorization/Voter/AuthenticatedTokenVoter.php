<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authorization\Voter;

use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\TrustResolver;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

class AuthenticatedTokenVoter extends AccessVoter
{
    const IS_AUTHENTICATED_FULLY = 'IS_AUTHENTICATED_FULLY';
    const IS_AUTHENTICATED_REMEMBERED = 'IS_AUTHENTICATED_REMEMBERED';
    const IS_AUTHENTICATED_ANONYMOUSLY = 'IS_AUTHENTICATED_ANONYMOUSLY';

    /**
     * @var TrustResolver
     */
    private $trustResolver;

    public function __construct(TrustResolver $trustResolver)
    {
        $this->trustResolver = $trustResolver;
    }

    public function vote(Tokenable $token, $subject, array $attributes): int
    {
        $vote = $this->abstain();

        foreach ($attributes as $attribute) {
            if (null === $attributes || (
                    $this->isAuthenticatedFully() !== $attribute &&
                    $this->isAuthenticatedRemembered() !== $attribute &&
                    $this->isAuthenticatedAnonymously() !== $attribute
                )) {
                continue;
            }

            $vote = $this->denied();

            if ($attribute === $this->isAuthenticatedFully() && $this->trustResolver->isFullFledged($token)) {
                return $this->granted();
            }

            if ($attribute === $this->isAuthenticatedRemembered() &&
                ($this->trustResolver->isRememberMe($token) || $this->trustResolver->isFullFledged($token))) {
                return $this->granted();
            }

            if ($attribute === $this->isAuthenticatedAnonymously() &&
                ($this->trustResolver->isAnonymous($token) ||
                    $this->trustResolver->isRememberMe($token) ||
                    $this->trustResolver->isFullFledged($token))) {
                return $this->granted();
            }
        }

        return $vote;
    }

    public function isAuthenticatedFully(): string
    {
        return static::IS_AUTHENTICATED_FULLY;
    }

    public function isAuthenticatedRemembered(): string
    {
        return static::IS_AUTHENTICATED_REMEMBERED;
    }

    public function isAuthenticatedAnonymously(): string
    {
        return static::IS_AUTHENTICATED_ANONYMOUSLY;
    }
}