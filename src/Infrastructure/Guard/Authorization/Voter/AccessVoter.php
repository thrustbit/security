<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authorization\Voter;

use Thrustbit\Security\Infrastructure\Guard\Authorization\Contract\Votable;

abstract class AccessVoter implements Votable
{
    protected function abstain(): int
    {
        return Votable::ACCESS_ABSTAIN;
    }

    protected function granted(): int
    {
        return Votable::ACCESS_GRANTED;
    }

    protected function denied(): int
    {
        return Votable::ACCESS_DENIED;
    }
}