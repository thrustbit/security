<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authorization\Voter;

use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\TrustResolver;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

class AnonymousVoter extends Voter
{
    /**
     * @var TrustResolver
     */
    private $trustResolver;

    public function __construct(TrustResolver $trustResolver)
    {
        $this->trustResolver = $trustResolver;
    }

    protected function supports($attribute, $subject): bool
    {
        return 'anonymous' === $attribute;
    }

    protected function voteOn(string $attribute, $subject, Tokenable $token): bool
    {
        return $this->trustResolver->isAnonymous($token);
    }
}