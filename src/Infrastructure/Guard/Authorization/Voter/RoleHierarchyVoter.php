<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authorization\Voter;

use Thrustbit\Security\Domain\Role\RoleHierarchy;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

class RoleHierarchyVoter extends RoleVoter
{
    /**
     * @var RoleHierarchy
     */
    private $roleHierarchy;

    public function __construct(RoleHierarchy $roleHierarchy, string $rolePrefix)
    {
        parent::__construct($rolePrefix);

        $this->roleHierarchy = $roleHierarchy;
    }

    protected function extractRoles(Tokenable $token): array
    {
        return $this->roleHierarchy->getReachableRoles($token->getRoles());
    }
}