<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Authorization\Voter;

use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

abstract class Voter extends AccessVoter
{
    public function vote(Tokenable $token, $subject, array $attributes): int
    {
        $vote = $this->abstain();

        foreach ($attributes as $attribute) {
            if (!$this->supports($attribute, $subject)) {
                continue;
            }

            $vote = $this->denied();

            if ($this->voteOn($attribute, $subject, $token)) {
                return $this->granted();
            }
        }

        return $vote;
    }

    abstract protected function supports($attribute, $subject): bool;

    abstract protected function voteOn(string $attribute, $subject, Tokenable $token): bool;
}