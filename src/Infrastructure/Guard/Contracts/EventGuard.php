<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Contracts;

use Illuminate\Http\Request;
use Thrustbit\Security\Domain\User\UserSecurity;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

interface EventGuard
{
    public function dispatch($event): void;

    public function dispatchLoginEvent(Request $request, Tokenable $token): void;

    public function dispatchLogoutEvent(Tokenable $token): void;

    public function dispatchImpersonateUserEvent(Request $request, UserSecurity $user): void;
}