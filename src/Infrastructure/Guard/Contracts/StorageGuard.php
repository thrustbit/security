<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Contracts;

use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

interface StorageGuard
{
    public function put(Tokenable $token): void;

    public function get(): ?Tokenable;

    public function required(): Tokenable;

    public function clear(): void;

    public function isEmpty(): bool;

    public function isNotEmpty(): bool;
}