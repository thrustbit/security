<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Guard\Contracts;

use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

interface Guard
{
    public function authenticate(Tokenable $token): Tokenable;

    public function storage(): StorageGuard;

    public function event(): EventGuard;
}