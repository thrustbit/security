<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Services\Recaller;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Application\Values\SecurityIdentifier;
use Thrustbit\Security\Domain\User\UserRecaller;
use Thrustbit\Security\Domain\User\UserSecurity;
use Thrustbit\Security\Domain\User\Values\UserId;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\RecallerToken;

class SimpleRecallerService extends RecallerService
{
    public function processAutoLogin(Recaller $recaller): Tokenable
    {
        // checkMe rotate token

        $user = $this->requireUserFromRecaller($recaller->id(), $recaller->token());

        $this->queue($user);

        return new RecallerToken($user, $this->providerKey, $this->recallerKey);
    }

    public function onLoginSuccess(Request $request, Response $response, Tokenable $token): void
    {
        $user = $this->refreshRememberToken($token->getUser());

        $this->queue($user);
    }

    public function onLoginFail(Request $request): void
    {
    }

    protected function requireUserFromRecaller(string $identifier, string $token): UserSecurity
    {
        return $this->recallerProvider->requireByRecallerIdentifier(
            $this->makeIdentifier($identifier),
            $token
        );
    }

    protected function refreshRememberToken(UserSecurity $user): UserSecurity
    {
        if (!$user instanceof UserRecaller) {
            throw new \RuntimeException(
                sprintf('Class "%s" must implement a recaller contract.', get_class($user)));
        }

        return $this->recallerProvider->refreshRecaller($user, $this->createRecallerTokenString());
    }

    protected function createRecallerTokenString(): string
    {
        return str_random(60);
    }

    protected function makeIdentifier(string $identifier): SecurityIdentifier
    {
        return UserId::fromString($identifier);
    }
}