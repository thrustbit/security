<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Services\Recaller;

use Illuminate\Contracts\Cookie\QueueingFactory;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Application\Exceptions\AuthenticationException;
use Thrustbit\Security\Application\Values\SecurityKey\ProviderKey;
use Thrustbit\Security\Application\Values\SecurityKey\RecallerKey;
use Thrustbit\Security\Domain\User\Exceptions\CookieTheft;
use Thrustbit\Security\Domain\User\UserSecurity;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\RecallerProvider;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;
use Thrustbit\Security\Infrastructure\Services\Logout\Contract\Logout;
use Thrustbit\Security\Infrastructure\Services\Recaller\Contract\Recallable;

abstract class RecallerService implements Recallable, Logout
{
    use HasCookie;

    /**
     * @var RecallerKey
     */
    protected $recallerKey;

    /**
     * @var ProviderKey
     */
    protected $providerKey;

    /**
     * @var RecallerProvider
     */
    protected $recallerProvider;

    public function __construct(RecallerKey $recallerKey,
                                ProviderKey $providerKey,
                                QueueingFactory $cookieJar,
                                RecallerProvider $recallerProvider)
    {
        $this->recallerKey = $recallerKey;
        $this->providerKey = $providerKey;
        $this->recallerProvider = $recallerProvider;

        $this->setCookieJar($cookieJar);
    }

    public function autoLogin(Request $request): ?Tokenable
    {
        if (!$recaller = $this->getRecaller($request)) {
            return null;
        }

        try {
            return $this->processAutoLogin($recaller);
        } catch (CookieTheft $cookieTheft) {
            $this->cancelCookie($request);

            throw $cookieTheft;
        } catch (AuthenticationException $authenticationException) {
            $this->cancelCookie($request);

            return null;
        }

        // checkMe cancel cookie over Exception ?
    }

    public function loginFail(Request $request): void
    {
        $this->cancelCookie($request);

        $this->onLoginFail($request);
    }

    public function loginSuccess(Request $request, Response $response, Tokenable $token): void
    {
        $this->cancelCookie($request);

        if (!$token->getUser() instanceof UserSecurity || !$this->isRememberMeRequested($request)) {
            return;
        }

        $this->onLoginSuccess($request, $response, $token);
    }

    public function logout(Request $request, Response $response, Tokenable $token): void
    {
        $this->cancelCookie($request);
    }

    protected function isRememberMeRequested(Request $request): bool
    {
        return
            $request->isMethod('post') &&
            in_array($request->input('remember-me'), [
                'true', 'yes', '1', 'on', 'remember-me'
            ], true);
    }

    abstract public function processAutoLogin(Recaller $recaller): Tokenable;

    abstract public function onLoginSuccess(Request $request, Response $response, Tokenable $token): void;

    abstract public function onLoginFail(Request $request): void;
}