<?php

namespace Thrustbit\Security\Infrastructure\Services\Recaller;

use Illuminate\Contracts\Cookie\QueueingFactory;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Cookie;
use Thrustbit\Security\Application\Exceptions\AuthenticationException;
use Thrustbit\Security\Application\Exceptions\Service\InvalidArgument;
use Thrustbit\Security\Domain\User\UserRecaller;
use Thrustbit\Security\Domain\User\UserSecurity;

trait HasCookie
{
    /**
     * @var QueueingFactory
     */
    private $cookieJar;

    protected function setCookieJar(QueueingFactory $cookieJar): void
    {
        $this->cookieJar = $cookieJar;
    }

    protected function createCookie(string $value): Cookie
    {
        return $this->cookieJar->forever($this->getName(), $value);
    }

    protected function cancelCookie(Request $request): void
    {
        if ($this->getRecaller($request)) {
            $this->cookieJar->queue(
                $this->cookieJar->forget($this->getName()));
        }
    }

    protected function queue(UserSecurity $user): void
    {
        if (!$user instanceof UserRecaller) {
            throw InvalidArgument::reason(
                sprintf('User must implement %s to use recaller service', UserRecaller::class));
        }

        $value = $user->getId()->read() . Recaller::DELIMITER . $user->getRecallerToken();

        $this->cookieJar->queue($this->createCookie($value));
    }

    protected function getRecaller(Request $request): ?Recaller
    {
        if (null === $recaller = $request->cookie($this->getName())) {
            return null;
        }

        $recaller = new Recaller($recaller);

        if ($recaller->valid()) {
            return $recaller;
        }

        throw new AuthenticationException('Recaller validation failed.');
    }

    protected function getName(): string
    {
        return 'remember_' . $this->recallerKey . '_' . sha1(static::class);
    }
}