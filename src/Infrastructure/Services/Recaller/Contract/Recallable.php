<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Services\Recaller\Contract;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

interface Recallable
{
    public function autoLogin(Request $request): ?Tokenable;

    public function loginFail(Request $request): void;

    public function loginSuccess(Request $request, Response $response, Tokenable $token): void;
}