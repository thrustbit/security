<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Services\Logout\Contract;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

interface Logout
{
    public function logout(Request $request, Response $response, Tokenable $token): void;
}