<?php

declare(strict_types=1);

namespace Thrustbit\Security\Infrastructure\Services\Logout;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;
use Thrustbit\Security\Infrastructure\Services\Logout\Contract\Logout;

class SessionLogout implements Logout
{
    public function logout(Request $request, Response $response, Tokenable $token): void
    {
        $request->session()->flush();
    }
}