<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Exceptions;

use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

interface SecurityException
{
    public function setToken(Tokenable $token): void;

    public function getToken(): ?Tokenable;
}