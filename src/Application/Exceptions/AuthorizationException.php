<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Exceptions;

class AuthorizationException extends SecurityDomainException
{
}