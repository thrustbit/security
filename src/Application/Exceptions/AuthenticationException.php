<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Exceptions;

use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

class AuthenticationException extends SecurityDomainException
{
    /**
     * @var Tokenable
     */
    protected $token;
}