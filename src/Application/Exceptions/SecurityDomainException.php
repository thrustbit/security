<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Exceptions;

use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

abstract class SecurityDomainException extends \RuntimeException implements SecurityException
{
    /**
     * @var Tokenable|null
     */
    protected $token;

    public function getToken(): ?Tokenable
    {
        return $this->token;
    }

    public function setToken(Tokenable $token): void
    {
        $this->token = $token;
    }
}