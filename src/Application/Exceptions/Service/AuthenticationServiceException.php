<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Exceptions\Service;

use Thrustbit\Security\Application\Exceptions\AuthenticationException;

class AuthenticationServiceException extends AuthenticationException
{
}