<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Exceptions\Service;

use Thrustbit\Security\Application\Values\SecurityIdentifier;

class InvalidArgument extends AuthenticationServiceException
{
    public static function unknownIdentifier($identifier): InvalidArgument
    {
        return new static(
            sprintf('Unknown identifier or unsupported identifier %s',
                $identifier instanceof SecurityIdentifier
                    ? $identifier->read()
                    : gettype($identifier)
            ));
    }

    public static function reason(string $message): InvalidArgument
    {
        return new self($message);
    }
}