<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Exceptions\Service;

class CredentialsNotFound extends AuthenticationServiceException
{
    // checkMe must be raised when token not found in storage
}