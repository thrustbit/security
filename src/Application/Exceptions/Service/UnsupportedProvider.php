<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Exceptions\Service;

use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\Authenticatable;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\AuthenticationProvider;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

class UnsupportedProvider extends AuthenticationServiceException
{
    public static function withSupport(Authenticatable $provider, Tokenable $token): UnsupportedProvider
    {
        if ($provider instanceof AuthenticationProvider) {
            return new self(
                sprintf('Authentication provider "%s" does not support token "%s"',
                    get_class($provider), get_class($token))
            );
        }

        return new self(sprintf('No authentication provider supports token %s', get_class($token)));
    }
}