<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Http\Request\Firewall\Contract;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Application\Exceptions\AuthenticationException;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

interface AuthenticationResponse
{
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception = null): Response;

    public function onAuthenticationSuccess(Request $request, Tokenable $token): Response;
}