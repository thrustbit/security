<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Http\Request\Firewall\Contract;

use Illuminate\Http\Request as IlluminateRequest;
use Symfony\Component\HttpFoundation\RequestMatcherInterface;

interface AuthenticationRequest extends RequestMatcherInterface
{
    public function extract(IlluminateRequest $request);
}