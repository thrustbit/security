<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Http\Request\Firewall;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Application\Exceptions\AuthenticationException;
use Thrustbit\Security\Application\Http\Request\Firewall\Contract\AuthenticationRequest;
use Thrustbit\Security\Application\Http\Response\Authentication\Contract\AuthenticationFailure;
use Thrustbit\Security\Application\Http\Response\Authentication\Contract\AuthenticationSuccess;
use Thrustbit\Security\Application\Values\SecurityKey\ProviderKey;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\SimplePreAuthenticator;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;
use Thrustbit\Security\Infrastructure\Guard\Contracts\Guard;

final class SimplePreAuthenticationFirewall extends AuthenticationFirewall
{
    /**
     * @var Guard
     */
    private $guard;

    /**
     * @var SimplePreAuthenticator
     */
    private $authenticator;

    /**
     * @var AuthenticationRequest
     */
    private $authenticationRequest;

    /**
     * @var ProviderKey
     */
    private $providerKey;

    /**
     * @var bool
     */
    private $stateless;

    public function __construct(Guard $guard,
                                SimplePreAuthenticator $authenticator,
                                AuthenticationRequest $authenticationRequest,
                                ProviderKey $providerKey,
                                bool $stateless)
    {
        $this->guard = $guard;
        $this->authenticator = $authenticator;
        $this->authenticationRequest = $authenticationRequest;
        $this->providerKey = $providerKey;
        $this->stateless = $stateless;
    }

    public function isRequired(Request $request): bool
    {
        return $this->guard->storage()->isEmpty()
            && $this->authenticationRequest->matches($request);
    }

    public function processAuthentication(Request $request): ?Response
    {
        try {
            $token = $this->authenticator->createToken($request, $this->providerKey);

            return $this->onSuccess(
                $request,
                $this->guard->authenticate($token)
            );
        } catch (AuthenticationException $exception) {
            return $this->onFailure($request, $exception);
        }
    }

    private function onSuccess(Request $request, Tokenable $token): ?Response
    {
        $response = null;

        if ($this->authenticator instanceof AuthenticationSuccess) {
            $response = $this->authenticator->onAuthenticationSuccess($request, $token);
        }

        if (!$this->stateless) {
            $this->guard->event()->dispatchLoginEvent($request, $token);
        }

        if (!$response) {
            $this->guard->storage()->put($token); // checkMe
        }

        return $response;
    }

    private function onFailure(Request $request, AuthenticationException $exception): ?Response
    {
        if ($this->authenticator instanceof AuthenticationFailure) {
            return $this->authenticator->onAuthenticationFailure($request, $exception);
        }

        return null;
    }
}