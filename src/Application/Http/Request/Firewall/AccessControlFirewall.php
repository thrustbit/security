<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Http\Request\Firewall;

use Illuminate\Http\Request;
use Thrustbit\Security\Infrastructure\Guard\Authorizer;
use Thrustbit\Security\Infrastructure\Guard\Contracts\Guard;

final class AccessControlFirewall
{
    /**
     * @var Guard
     */
    private $guard;

    /**
     * @var Authorizer
     */
    private $authorizer;

    /**
     * @var array
     */
    private $attributes;

    public function __construct(Guard $guard, Authorizer $authorizer, array $attributes = [])
    {
        $this->guard = $guard;
        $this->authorizer = $authorizer;
        $this->attributes = $attributes;
    }

    public function handle(Request $request, \Closure $next)
    {
        $token = $this->guard->storage()->required();

        if (!$this->attributes) {
            return $next($request);
        }

        if (!$token->isAuthenticated()) {
            $this->guard->storage()->put(
                $token = $this->guard->authenticate($token)
            );
        }

        $this->authorizer->requireGranted($token, $this->attributes, $request);

        return $next($request);
    }

    public function setAttributes(array $attributes)
    {
        $this->attributes = $attributes;
    }
}