<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Http\Request\Firewall;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Application\Exceptions\AuthenticationException;
use Thrustbit\Security\Infrastructure\Guard\Contracts\Guard;
use Thrustbit\Security\Infrastructure\Services\Recaller\Contract\Recallable;

final class RecallerFirewall extends AuthenticationFirewall
{
    /**
     * @var Recallable
     */
    private $recallerService;

    /**
     * @var Guard
     */
    private $guard;

    public function __construct(Recallable $recallerService, Guard $guard)
    {
        $this->recallerService = $recallerService;
        $this->guard = $guard;
    }

    public function isRequired(Request $request): bool
    {
        return $this->guard->storage()->isEmpty();
    }

    public function processAuthentication(Request $request): ?Response
    {
        if (!$token = $this->recallerService->autoLogin($request)) {
            return null;
        }

        try {
            $this->guard->storage()->put(
                $token = $this->guard->authenticate($token)
            );

            $this->guard->event()->dispatchLoginEvent($request, $token);

            return null;
        } catch (AuthenticationException $exception) {
            $this->recallerService->loginFail($request);

            throw $exception;
        }
    }
}