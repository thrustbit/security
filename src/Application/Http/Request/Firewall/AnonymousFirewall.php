<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Http\Request\Firewall;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Application\Exceptions\AuthenticationException;
use Thrustbit\Security\Application\Values\SecurityKey\AnonymousKey;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\AnonymousToken;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Values\AnonymousIdentifier;
use Thrustbit\Security\Infrastructure\Guard\Contracts\Guard;

final class AnonymousFirewall extends AuthenticationFirewall
{
    /**
     * @var Guard
     */
    private $guard;

    /**
     * @var AnonymousKey
     */
    private $anonymousKey;

    public function __construct(Guard $guard, AnonymousKey $anonymousKey)
    {
        $this->guard = $guard;
        $this->anonymousKey = $anonymousKey;
    }

    public function isRequired(Request $request): bool
    {
        return $this->guard->storage()->isEmpty();
    }

    public function processAuthentication(Request $request): ?Response
    {
        $token = new AnonymousToken(new AnonymousIdentifier(), $this->anonymousKey);

        try {
            $this->guard->storage()->put(
                $this->guard->authenticate($token)
            );
        } catch (AuthenticationException $exception) {
        }

        return null;
    }
}