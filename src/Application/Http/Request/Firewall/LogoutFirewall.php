<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Http\Request\Firewall;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Application\Http\Request\Firewall\Contract\AuthenticationRequest;
use Thrustbit\Security\Application\Http\Response\Logout\Contract\LogoutSuccess;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\TrustResolver;
use Thrustbit\Security\Infrastructure\Guard\Contracts\Guard;
use Thrustbit\Security\Infrastructure\Services\Logout\Contract\Logout;

final class LogoutFirewall extends AuthenticationFirewall
{
    /**
     * @var Guard
     */
    private $guard;

    /**
     * @var LogoutSuccess
     */
    private $redirect;

    /**
     * @var TrustResolver
     */
    private $trustResolver;

    /**
     * @var AuthenticationRequest
     */
    private $authenticationRequest;

    /**
     * @var array
     */
    private $logoutHandlers = [];

    public function __construct(Guard $guard,
                                LogoutSuccess $redirect,
                                TrustResolver $trustResolver,
                                AuthenticationRequest $authenticationRequest)
    {
        $this->guard = $guard;
        $this->redirect = $redirect;
        $this->trustResolver = $trustResolver;
        $this->authenticationRequest = $authenticationRequest;
    }

    public function isRequired(Request $request): bool
    {
        if ($this->guard->storage()->isEmpty() || $this->trustResolver->isAnonymous($this->guard->storage()->get())) {
            return false;
        }

        return $this->authenticationRequest->matches($request);
    }

    public function processAuthentication(Request $request): ?Response
    {
        $response = $this->redirect->onLogoutSuccess($request);
        $token = $this->guard->storage()->get();

        /** @var Logout $logoutHandler */
        foreach ($this->logoutHandlers as $logoutHandler) {
            $logoutHandler->logout($request, $response, $token);
        }

        $this->guard->event()->dispatchLogoutEvent($token);

        $this->guard->storage()->clear();

        return $response;
    }

    public function addHandler(Logout $logoutHandler): void
    {
        $this->logoutHandlers [] = $logoutHandler;
    }
}