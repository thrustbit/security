<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Http\Request\Firewall;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Http\Request;
use Thrustbit\Security\Application\Exceptions\Service\InvalidArgument;
use Thrustbit\Security\Application\Exceptions\Service\UnsupportedUser;
use Thrustbit\Security\Application\Http\Request\Event\ContextEventable;
use Thrustbit\Security\Application\Values\SecurityKey\ContextKey;
use Thrustbit\Security\Domain\User\Exceptions\UserNotFound;
use Thrustbit\Security\Domain\User\Provider\UserProvider;
use Thrustbit\Security\Domain\User\UserSecurity;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Storage;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

final class ContextControlFirewall
{
    /**
     * @var Storage
     */
    private $tokenStorage;

    /**
     * @var ContextKey
     */
    private $contextKey;

    /**
     * @var Dispatcher
     */
    private $dispatcher;

    /**
     * @var array[UserProvider]
     */
    private $userProviders;

    /**
     * @var ContextEventable
     */
    private $event;

    public function __construct(Storage $tokenStorage,
                                ContextKey $contextKey,
                                Dispatcher $dispatcher,
                                array $userProviders)
    {
        if (empty($userProviders)) {
            throw InvalidArgument::reason('Provide at least one user provider for context firewall');
        }

        $this->tokenStorage = $tokenStorage;
        $this->contextKey = $contextKey;
        $this->dispatcher = $dispatcher;
        $this->userProviders = $userProviders;
        $this->event = new ContextEventable($contextKey);
    }

    public function handle(Request $request, \Closure $next)
    {
        $this->dispatcher->dispatch($this->event);

        $tokenString = $request->session()->get($this->event->sessionKey());

        if ($tokenString) {
            $token = unserialize($tokenString, [Tokenable::class]);

            if ($token instanceof Tokenable) {
                $this->tokenStorage->setToken(
                    $this->refreshUser($token)
                );
            } else {
                $this->tokenStorage->setToken(null);
            }
        }

        return $next($request);
    }

    private function refreshUser(Tokenable $token): ?Tokenable
    {
        $user = $token->getUser();

        if (!$user instanceof UserSecurity) {
            return $token;
        }

        $notFound = false;
        $userClass = get_class($user);

        /** @var UserProvider $userProvider */
        foreach ($this->userProviders as $userProvider) {
            if (!$userProvider->supportsClass($userClass)) {
                continue;
            }

            try {
                $refreshedUser = $userProvider->refreshUser($user);

                $token->setUser($refreshedUser);

                return $token;
            } catch (UserNotFound $exception) {
                $notFound = true;
            }
        }

        if ($notFound) {
            return null;
        }

        throw UnsupportedUser::withUser($user);
    }
}