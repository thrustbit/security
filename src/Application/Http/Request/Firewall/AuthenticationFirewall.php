<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Http\Request\Firewall;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Application\Http\Request\Firewall\Contract\Firewall;

abstract class AuthenticationFirewall implements Firewall
{
    public function handle(Request $request, \Closure $next)
    {
        $response = null;

        if ($this->isRequired($request)) {
            $response = $this->processAuthentication($request);
        }

        return $response ?? $next($request);
    }

    abstract public function isRequired(Request $request): bool;

    abstract public function processAuthentication(Request $request): ?Response;
}