<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Http\Request\Firewall;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Application\Exceptions\AuthenticationException;
use Thrustbit\Security\Application\Exceptions\SecurityValueFailed;
use Thrustbit\Security\Application\Http\Request\Firewall\Contract\AuthenticationRequest;
use Thrustbit\Security\Application\Http\Response\Entrypoint\Contract\Entrypoint;
use Thrustbit\Security\Application\Values\EmailAddress;
use Thrustbit\Security\Application\Values\SecurityIdentifier;
use Thrustbit\Security\Application\Values\SecurityKey\ProviderKey;
use Thrustbit\Security\Domain\User\Exceptions\BadCredentials;
use Thrustbit\Security\Domain\User\UserSecurity;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\UserNamePasswordToken;
use Thrustbit\Security\Infrastructure\Guard\Contracts\Guard;

final class HttpBasicFirewall extends AuthenticationFirewall
{
    /**
     * @var Guard
     */
    private $guard;

    /**
     * @var AuthenticationRequest
     */
    private $authenticationRequest;

    /**
     * @var Entrypoint
     */
    private $entrypoint;

    /**
     * @var ProviderKey
     */
    private $providerKey;

    public function __construct(Guard $guard,
                                AuthenticationRequest $authenticationRequest,
                                Entrypoint $entrypoint,
                                ProviderKey $providerKey)
    {
        $this->guard = $guard;
        $this->authenticationRequest = $authenticationRequest;
        $this->entrypoint = $entrypoint;
        $this->providerKey = $providerKey;
    }

    public function isRequired(Request $request): bool
    {
        try {
            [$identifier] = $this->authenticationRequest->extract($request);

            if (!$identifier) {
                return false;
            }
        } catch (SecurityValueFailed $exception) {
            return false;
        }

        return !$this->isAlreadyAuthenticated($identifier, $this->guard->storage()->get());
    }

    public function processAuthentication(Request $request): ?Response
    {
        try {
            $this->guard->storage()->put(
                $token = $this->guard->authenticate(
                    $this->createToken($request)
                ));

            $this->guard->event()->dispatchLoginEvent($request, $token);
        } catch (AuthenticationException | SecurityValueFailed $exception) {
            $this->guard->storage()->clear();

            return $this->entrypoint->startAuthentication($request, $exception);
        }

        return null;
    }

    protected function createToken(Request $request): UserNamePasswordToken
    {
        [$identifier, $password] = $this->authenticationRequest->extract($request);

        if (!$identifier || !$password) {
            throw new BadCredentials('Invalid credentials');
        }

        return new UserNamePasswordToken($identifier, $password, $this->providerKey);
    }

    protected function isAlreadyAuthenticated(SecurityIdentifier $identifier, Tokenable $token = null): bool
    {
        if(!$identifier instanceof EmailAddress){
            return false;
        }

        return $token instanceof UserNamePasswordToken
            && $token->isAuthenticated()
            && $token->getUser() instanceof UserSecurity
            && $token->getUser()->getEmail()->sameValueAs($identifier);
    }
}