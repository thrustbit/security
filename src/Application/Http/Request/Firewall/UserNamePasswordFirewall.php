<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Http\Request\Firewall;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Application\Exceptions\AuthenticationException;
use Thrustbit\Security\Application\Http\Request\Event\UserLoginFailed;
use Thrustbit\Security\Application\Http\Request\Firewall\Contract\AuthenticationRequest;
use Thrustbit\Security\Application\Http\Request\Firewall\Contract\AuthenticationResponse;
use Thrustbit\Security\Application\Values\SecurityKey\ProviderKey;
use Thrustbit\Security\Domain\User\Exceptions\BadCredentials;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\UserNamePasswordToken;
use Thrustbit\Security\Infrastructure\Guard\Contracts\Guard;
use Thrustbit\Security\Infrastructure\Services\Recaller\Contract\Recallable;

class UserNamePasswordFirewall extends AuthenticationFirewall
{
    /**
     * @var Recallable
     */
    protected $recaller;

    /**
     * @var Guard
     */
    protected $guard;

    /**
     * @var AuthenticationRequest
     */
    protected $authenticationRequest;

    /**
     * @var AuthenticationResponse
     */
    protected $authenticationResponse;

    /**
     * @var ProviderKey
     */
    protected $providerKey;

    /**
     * @var bool
     */
    protected $stateless;

    public function __construct(Guard $guard,
                                AuthenticationRequest $authenticationRequest,
                                AuthenticationResponse $authenticationResponse,
                                ProviderKey $providerKey,
                                bool $stateless)
    {
        $this->guard = $guard;
        $this->authenticationRequest = $authenticationRequest;
        $this->authenticationResponse = $authenticationResponse;
        $this->providerKey = $providerKey;
        $this->stateless = $stateless;
    }

    public function isRequired(Request $request): bool
    {
        return $this->authenticationRequest->matches($request);
    }

    public function processAuthentication(Request $request): ?Response
    {
        try {
            $token = $this->guard->authenticate($this->createToken($request));

            return $this->onSuccess($request, $token);
        } catch (AuthenticationException $exception) {
            return $this->onFailure($request, $exception);
        }
    }

    public function setRecaller(Recallable $recaller): void
    {
        $this->recaller = $recaller;
    }

    protected function onSuccess(Request $request, Tokenable $token): Response
    {
        $response = $this->authenticationResponse->onAuthenticationSuccess($request, $token);

        if ($this->recaller) {
            $this->recaller->loginSuccess($request, $response, $token);
        }

        if (!$this->stateless) {
            $this->guard->event()->dispatchLoginEvent($request, $token);
        }

        $this->guard->storage()->put($token);

        return $response;
    }

    protected function onFailure(Request $request, AuthenticationException $exception): Response
    {
        $this->guard->storage()->clear();

        $this->guard->event()->dispatch(new UserLoginFailed($request, $exception));

        return $this->authenticationResponse->onAuthenticationFailure($request, $exception);
    }

    protected function createToken(Request $request): UserNamePasswordToken
    {
        [$identifier, $password] = $this->authenticationRequest->extract($request);

        if (!$identifier || !$password) {
            throw new BadCredentials('Invalid credentials');
        }

        return new UserNamePasswordToken($identifier, $password, $this->providerKey);
    }
}