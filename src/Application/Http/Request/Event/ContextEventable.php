<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Http\Request\Event;

use Thrustbit\Security\Application\Values\SecurityKey\ContextKey;

class ContextEventable
{
    /**
     * @var ContextKey
     */
    protected $contextKey;

    /**
     * @var string
     */
    protected $sessionKey;

    public function __construct(ContextKey $contextKey)
    {
        $this->contextKey = $contextKey;
        $this->sessionKey = 'security_' . $contextKey->getKey();
    }

    public function contextKey(): ContextKey
    {
        return $this->contextKey;
    }

    public function sessionKey(): string
    {
        return $this->sessionKey;
    }
}