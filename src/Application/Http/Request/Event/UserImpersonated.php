<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Http\Request\Event;

use Illuminate\Http\Request;
use Thrustbit\Security\Domain\User\UserSecurity;

class UserImpersonated
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var UserSecurity
     */
    private $target;

    public function __construct(Request $request, UserSecurity $target)
    {
        $this->request = $request;
        $this->target = $target;
    }

    public function request(): Request
    {
        return $this->request;
    }

    public function target(): UserSecurity
    {
        return $this->target;
    }
}