<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Http\Request\Event;

use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

class UserLogout
{
    /**
     * @var Tokenable
     */
    private $token;

    public function __construct(Tokenable $token)
    {
        $this->token = $token;
    }

    public function getToken(): Tokenable
    {
        return $this->token;
    }
}