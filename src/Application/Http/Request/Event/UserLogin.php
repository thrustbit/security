<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Http\Request\Event;

use Illuminate\Http\Request;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

class UserLogin
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var Tokenable
     */
    private $token;

    public function __construct(Request $request, Tokenable $token)
    {
        $this->request = $request;
        $this->token = $token;
    }

    public function getToken(): Tokenable
    {
        return $this->token;
    }

    public function getRequest(): Request
    {
        return $this->request;
    }
}