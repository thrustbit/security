<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Http\Request\Event;

use Illuminate\Http\Request;
use Thrustbit\Security\Application\Exceptions\AuthenticationException;

class UserLoginFailed
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var AuthenticationException
     */
    private $exception;

    public function __construct(Request $request, AuthenticationException $exception = null)
    {
        $this->request = $request;
        $this->exception = $exception;
    }

    public function getRequest(): Request
    {
        return $this->request;
    }

    public function getException(): AuthenticationException
    {
        return $this->exception;
    }
}