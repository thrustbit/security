<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Http\Response\Logout\Contract;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

interface LogoutSuccess
{
    public function onLogoutSuccess(Request $request): Response;
}