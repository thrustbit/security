<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Http\Response\Logout;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Application\Http\Response\Logout\Contract\LogoutSuccess;

class DefaultLogoutSuccess implements LogoutSuccess
{
    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    /**
     * @var string
     */
    private $routeName;

    public function __construct(ResponseFactory $responseFactory, string $routeName)
    {
        $this->responseFactory = $responseFactory;
        $this->routeName = $routeName;
    }

    public function onLogoutSuccess(Request $request): Response
    {
        return $this->responseFactory->redirectToRoute($this->routeName);
    }
}