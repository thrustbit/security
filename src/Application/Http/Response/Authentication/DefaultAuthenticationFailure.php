<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Http\Response\Authentication;

use Illuminate\Http\Request;
use Illuminate\Routing\ResponseFactory;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Application\Exceptions\AuthenticationException;
use Thrustbit\Security\Application\Http\Response\Authentication\Contract\AuthenticationFailure;

class DefaultAuthenticationFailure implements AuthenticationFailure
{
    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    /**
     * @var string
     */
    private $routeName;

    public function __construct(ResponseFactory $responseFactory, string $routeName)
    {
        $this->responseFactory = $responseFactory;
        $this->routeName = $routeName;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception = null): Response
    {
        return $this->responseFactory->redirectToRoute($this->routeName)
            ->with('message', $exception ? $exception->getMessage() : 'Authentication failed');
    }
}