<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Http\Response\Authentication\Contract;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

interface AuthenticationSuccess
{
    public function onAuthenticationSuccess(Request $request, Tokenable $token): Response;
}