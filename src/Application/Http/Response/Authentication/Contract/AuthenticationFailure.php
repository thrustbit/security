<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Http\Response\Authentication\Contract;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Application\Exceptions\AuthenticationException;

interface AuthenticationFailure
{
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception = null): Response;
}