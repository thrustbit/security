<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Http\Response\Authentication;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Application\Exceptions\AuthenticationException;
use Thrustbit\Security\Application\Http\Request\Firewall\Contract\AuthenticationResponse;
use Thrustbit\Security\Application\Http\Response\Authentication\Contract\AuthenticationFailure;
use Thrustbit\Security\Application\Http\Response\Authentication\Contract\AuthenticationSuccess;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

class DefaultAuthenticationResponse implements AuthenticationResponse, AuthenticationFailure, AuthenticationSuccess
{
    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    /**
     * @var string
     */
    private $failureRoute;

    /**
     * @var string
     */
    private $successRoute;

    public function __construct(ResponseFactory $responseFactory, string $failureRoute, string $successRoute)
    {
        $this->responseFactory = $responseFactory;
        $this->failureRoute = $failureRoute;
        $this->successRoute = $successRoute;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception = null): Response
    {
        return $this->responseFactory->redirectToRoute($this->failureRoute)
            ->with('message', $exception ? $exception->getMessage() : 'Authentication failed');
    }

    public function onAuthenticationSuccess(Request $request, Tokenable $token): Response
    {
        return $this->responseFactory->redirectToRoute($this->successRoute)
            ->with('message', 'Authentication succeeded');
    }
}