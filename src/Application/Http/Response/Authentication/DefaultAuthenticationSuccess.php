<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Http\Response\Authentication;

use Illuminate\Http\Request;
use Illuminate\Routing\ResponseFactory;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Application\Http\Response\Authentication\Contract\AuthenticationSuccess;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

class DefaultAuthenticationSuccess implements AuthenticationSuccess
{
    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    /**
     * @var string
     */
    private $routeName;

    public function __construct(ResponseFactory $responseFactory, string $routeName)
    {
        $this->responseFactory = $responseFactory;
        $this->routeName = $routeName;
    }

    public function onAuthenticationSuccess(Request $request, Tokenable $token): Response
    {
        return $this->responseFactory->redirectToRoute($this->routeName)
            ->with('message', 'Authentication success');
    }
}