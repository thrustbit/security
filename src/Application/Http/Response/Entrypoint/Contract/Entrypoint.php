<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Http\Response\Entrypoint\Contract;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Application\Exceptions\AuthenticationException;

interface Entrypoint
{
    public function startAuthentication(Request $request, AuthenticationException $exception = null): Response;
}