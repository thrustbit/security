<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Http\Response\Entrypoint;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\Factory as ViewFactory;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Application\Exceptions\AuthenticationException;
use Thrustbit\Security\Application\Http\Response\Entrypoint\Contract\Entrypoint;

class HttpBasicEntrypoint implements Entrypoint
{
    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    /**
     * @var ViewFactory
     */
    private $viewFactory;

    /**
     * @var string
     */
    private $realmName;

    public function __construct(ResponseFactory $responseFactory, ViewFactory $viewFactory, string $realmName = 'Private access')
    {
        $this->responseFactory = $responseFactory;
        $this->viewFactory = $viewFactory;
        $this->realmName = $realmName;
    }

    public function startAuthentication(Request $request, AuthenticationException $exception = null): Response
    {
        $statusCode = Response::HTTP_UNAUTHORIZED;

        $content = $this->viewFactory->make('errors.' . $statusCode);

        $headers = ['WWW-Authenticate' => sprintf('Basic realm="%s"', $this->realmName)];

        return $this->responseFactory->make($content, $statusCode, $headers);
    }
}