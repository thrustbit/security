<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Http\Response\Entrypoint;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Application\Exceptions\AuthenticationException;
use Thrustbit\Security\Application\Http\Response\Entrypoint\Contract\Entrypoint;

class DefaultFormEntrypoint implements Entrypoint
{
    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    /**
     * @var string
     */
    private $routeName;

    public function __construct(ResponseFactory $responseFactory, string $routeName)
    {
        $this->responseFactory = $responseFactory;
        $this->routeName = $routeName;
    }

    public function startAuthentication(Request $request, AuthenticationException $exception = null): Response
    {
        return $this->responseFactory->redirectToRoute($this->routeName)
            ->with('message', $exception ? $exception->getMessage() : 'Login required');
    }
}