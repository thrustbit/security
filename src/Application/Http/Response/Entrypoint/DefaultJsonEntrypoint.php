<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Http\Response\Entrypoint;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Application\Exceptions\AuthenticationException;
use Thrustbit\Security\Application\Http\Response\Entrypoint\Contract\Entrypoint;

class DefaultJsonEntrypoint implements Entrypoint
{
    public function startAuthentication(Request $request, AuthenticationException $exception = null): Response
    {
        return new JsonResponse(
            [
                'message' => $exception ? $exception->getMessage() : 'Login required'
            ]
        );
    }
}