<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Http\Response\Authorization\Contract;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Application\Exceptions\AuthorizationException;

interface AuthorizationDenied
{
    public function handle(Request $request, AuthorizationException $exception): Response;
}