<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Http\Response\Authorization;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Application\Exceptions\AuthorizationException;
use Thrustbit\Security\Application\Http\Response\Authorization\Contract\AuthorizationDenied;

class DefaultJsonAuthorizationDenied implements AuthorizationDenied
{
    public function handle(Request $request, AuthorizationException $exception): Response
    {
        $statusCode = Response::HTTP_UNAUTHORIZED;

        return new JsonResponse(
            [
                'message' => $exception->getMessage() ?? 'Authorization denied',
                'http_code' => $statusCode
            ], $statusCode);
    }
}