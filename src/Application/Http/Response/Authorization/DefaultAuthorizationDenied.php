<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Http\Response\Authorization;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Application\Exceptions\AuthorizationException;
use Thrustbit\Security\Application\Http\Response\Authorization\Contract\AuthorizationDenied;

class DefaultAuthorizationDenied implements AuthorizationDenied
{
    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    /**
     * @var string
     */
    private $routeName;

    public function __construct(ResponseFactory $responseFactory, string $routeName)
    {
        $this->responseFactory = $responseFactory;
        $this->routeName = $routeName;
    }

    public function handle(Request $request, AuthorizationException $exception): Response
    {
        return $this->responseFactory->redirectToRoute($this->routeName)
            ->with('message', $exception->getMessage() ?? 'Authorization denied');
    }
}