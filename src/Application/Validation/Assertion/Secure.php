<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Validation\Assertion;

use Assert\Assertion;
use Thrustbit\Security\Application\Exceptions\SecurityValueFailed;

class Secure extends Assertion
{
    /**
     * @var string
     */
    protected static $exceptionClass = SecurityValueFailed::class;
}