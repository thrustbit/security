<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Values;

interface EncodedPassword extends Credential
{
}