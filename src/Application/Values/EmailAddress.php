<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Values;

interface EmailAddress extends SecurityIdentifier
{
}