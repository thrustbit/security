<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Values;

use Ramsey\Uuid\UuidInterface;
use Thrustbit\Security\Application\Validation\Assertion\Secure;

abstract class Uuid implements Uid
{
    /**
     * @var UuidInterface
     */
    protected $uuid;

    /**
     * @var string
     */
    protected static $message = 'Unique identifier is not valid';

    protected function __construct(UuidInterface $uuid)
    {
        $this->uuid = $uuid;
    }

    protected static function validate($uid): void
    {
        Secure::string($uid, static::$message);
        Secure::notEmpty($uid, static::$message);
        Secure::uuid($uid, static::$message);
    }

    public function read(): string
    {
        return $this->uuid->toString();
    }

    public function getUid(): UuidInterface
    {
        return $this->uuid;
    }

    public function __toString(): string
    {
        return $this->uuid->toString();
    }
}