<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Values;

interface SecurityValue
{
    public function sameValueAs(SecurityValue $aValue): bool;
}