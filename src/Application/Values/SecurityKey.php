<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Values;

interface SecurityKey extends SecurityValue
{
    public function getKey(): string;
}