<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Values;

interface SecurityIdentifier extends SecurityValue
{
    /**
     * @return mixed
     */
    public function read();
}