<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Values;

interface Credential extends SecurityValue
{
    public function getCredential();
}