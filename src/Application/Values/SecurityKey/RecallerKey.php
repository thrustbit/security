<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Values\SecurityKey;

use Thrustbit\Security\Application\Values\SecurityValue;

final class RecallerKey extends SecurityKey
{
    public function sameValueAs(SecurityValue $aValue): bool
    {
        return $aValue instanceof $this && $this->getKey() === $aValue->getKey();
    }
}