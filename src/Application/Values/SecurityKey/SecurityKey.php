<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Values\SecurityKey;

use Thrustbit\Security\Application\Validation\Assertion\Secure;
use Thrustbit\Security\Application\Values\SecurityKey as SecureKey;

abstract class SecurityKey implements SecureKey
{
    /**
     * @var string
     */
    private $key;

    public function __construct($key)
    {
        Secure::string($key);
        Secure::notEmpty($key);

        $this->key = $key;
    }

    public function getKey(): string
    {
       return $this->key;
    }

    public function __toString(): string
    {
        return $this->key;
    }
}