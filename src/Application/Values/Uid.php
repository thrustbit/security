<?php

declare(strict_types=1);

namespace Thrustbit\Security\Application\Values;

use Ramsey\Uuid\UuidInterface;

interface Uid extends SecurityIdentifier
{
    public function getUid(): UuidInterface;
}