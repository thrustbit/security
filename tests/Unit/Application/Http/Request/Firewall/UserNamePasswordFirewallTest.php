<?php

declare(strict_types=1);

namespace ThrustbitTests\Security\Unit\Application\Http\Request\Firewall;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Thrustbit\Security\Application\Exceptions\AuthenticationException;
use Thrustbit\Security\Application\Http\Request\Firewall\UserNamePasswordFirewall;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Values\SimpleCredentialFactory;
use Thrustbit\Security\Infrastructure\Services\Recaller\Contract\Recallable;
use ThrustbitTests\Security\Mock\SomeUserIdentifier;

class UserNamePasswordFirewallTest extends FirewallTestCase
{
    private $token;

    public function setUp()
    {
        parent::setUp();

        $this->token = $this->getMockForAbstractClass(Tokenable::class);
    }

    /**
     * @test
     */
    public function it_determine_if_authentication_is_needed(): void
    {
        $this->authenticationRequest->expects($this->once())->method('matches')->willReturn(false);
        $this->guard->expects($this->never())->method('authenticate');

        $this->throughFirewall();
    }

    /**
     * @test
     */
    public function it_return_response_on_authentication_success(): void
    {
        $this->matchAndValidAuthenticationRequest();
        $this->guard->expects($this->once())->method('authenticate')->willReturn($this->token);

        $this->assertFalse($this->stateless);
        $this->guard->expects($this->once())->method('dispatchLoginEvent');
        $this->guard->expects($this->once())->method('setToken');

        $mock = $this->expectedAuthenticationResponse(true, 'foo');

        $this->assertSame($mock, $this->throughFirewall());
    }

    /**
     * @test
     */
    public function it_does_not_dispatch_login_event_on_authentication_success_with_stateless_request(): void
    {
        $this->matchAndValidAuthenticationRequest();
        $this->guard->expects($this->once())->method('authenticate')->willReturn($this->token);

        $this->stateless = true; // checkMe
        $this->assertTrue($this->stateless);
        $this->guard->expects($this->never())->method('dispatchLoginEvent');
        $this->guard->expects($this->once())->method('setToken');

        $mock = $this->expectedAuthenticationResponse(true, 'foo');

        $this->assertSame($mock, $this->throughFirewall());
    }

    /**
     * @test
     *
     * @param array $identifiers
     * @dataProvider giveInvalidCredentialsFromRequest
     */
    public function it_return_response_with_invalid_credentials_from_authentication_request(array $identifiers): void
    {
        $this->authenticationRequest->expects($this->once())->method('matches')->willReturn(true);
        $this->authenticationRequest->expects($this->once())->method('extract')->willReturn($identifiers);

        $this->guard->expects($this->never())->method('authenticate');
        $this->guard->expects($this->once())->method('clearStorage');

        $mock = $this->expectedAuthenticationResponse(false, 'foo');

        $this->assertSame($mock, $this->throughFirewall());
    }

    /**
     * @test
     */
    public function it_return_response_on_authentication_failure(): void
    {
        $this->matchAndValidAuthenticationRequest();

        $exception = new AuthenticationException('foo');

        $this->guard->expects($this->once())->method('authenticate')->willThrowException($exception);
        $this->guard->expects($this->once())->method('clearStorage');

        $mock = $this->expectedAuthenticationResponse(false, 'foo');

        $this->assertSame($mock, $this->throughFirewall());
    }

    /**
     * @test
     *
     * @expectedException \RuntimeException
     * @expectedExceptionMessage bar
     */
    public function it_handle_only_authentication_exception(): void
    {
        $this->matchAndValidAuthenticationRequest();

        $this->guard->expects($this->once())->method('authenticate')
            ->willThrowException(new \RuntimeException('bar'));

        $this->throughFirewall();
    }

    /**
     * @test
     */
    public function it_make_aware_of_recaller_service_on_authentication_success(): void
    {
        $this->matchAndValidAuthenticationRequest();
        $this->guard->expects($this->once())->method('authenticate')->willReturn($this->token);

        $mock = $this->expectedAuthenticationResponse(true, 'foo');

        $recallerService = $this->getMockForAbstractClass(Recallable::class);
        $recallerService->expects($this->once())->method('loginSuccess');

        $firewall = $this->getFirewall();
        $firewall->setRecaller($recallerService);

        $request = $this->getMockForAbstractClass(Request::class);
        $response = $firewall->handle($request, function () {
        });

        $this->assertSame($mock, $response);
    }

    protected function getFirewall(): UserNamePasswordFirewall
    {
        return new UserNamePasswordFirewall(
            $this->guard, $this->authenticationRequest, $this->authenticationResponse, $this->providerKey, $this->stateless
        );
    }

    private function matchAndValidAuthenticationRequest(): void
    {
        $this->authenticationRequest->expects($this->once())->method('matches')->willReturn(true);

        $this->giveValidIdentifiersFromAuthenticationRequest();
    }

    private function expectedAuthenticationResponse(bool $success, string $message): Response
    {
        $response = new Response($message);

        if ($success) {
            $this->authenticationResponse->expects($this->once())->method('onAuthenticationSuccess')
                ->willReturn($response);
            return $response;
        }

        $this->authenticationResponse->expects($this->once())->method('onAuthenticationFailure')
            ->willReturn($response);

        return $response;
    }

    private function giveValidIdentifiersFromAuthenticationRequest(): void
    {
        $this->authenticationRequest->expects($this->once())->method('extract')
            ->willReturn(
                [new SomeUserIdentifier(true), SimpleCredentialFactory::fromString('foobarfoo')]
            );
    }

    public function giveInvalidCredentialsFromRequest(): array
    {
        return [
            [[null, 'foo'], [null, null], ['foo', null], ['', ''],]
        ];
    }

}