<?php

declare(strict_types=1);

namespace ThrustbitTests\Security\Unit\Application\Http\Request\Firewall;

use Thrustbit\Security\Application\Exceptions\AuthenticationException;
use Thrustbit\Security\Application\Http\Request\Firewall\AnonymousFirewall;
use Thrustbit\Security\Application\Values\SecurityKey\AnonymousKey;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;

class AnonymousFirewallTest extends FirewallTestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    /**
     * @test
     */
    public function it_determine_if_authentication_is_not_required(): void
    {
        $this->guard->expects($this->once())->method('isStorageEmpty')->willReturn(false);
        $this->guard->expects($this->never())->method('authenticate');

        $this->assertNull($this->throughFirewall());
    }

    /**
     * @test
     */
    public function it_return_null_response_on_authentication_success(): void
    {
        $this->guard->expects($this->once())->method('isStorageEmpty')->willReturn(true);
        $token = $this->getMockForAbstractClass(Tokenable::class);
        $this->guard->expects($this->once())->method('authenticate')->willReturn($token);
        $this->guard->expects($this->once())->method('setToken');

        $this->assertNull($this->throughFirewall());
    }

    /**
     * @test
     */
    public function it_catch_authentication_exception_on_authentication_failure(): void
    {
        $this->guard->expects($this->once())->method('isStorageEmpty')->willReturn(true);

        $exception = new AuthenticationException('foo');
        $this->guard->expects($this->once())->method('authenticate')->willThrowException($exception);
        $this->guard->expects($this->never())->method('setToken');

        $this->assertNull($this->throughFirewall());
    }

    /**
     * @test
     */
    public function it_raise_non_authentication_exception_on_authentication_failure(): void
    {
        $this->guard->expects($this->once())->method('isStorageEmpty')->willReturn(true);

        $exception = new \RuntimeException('foo');
        $this->guard->expects($this->once())->method('authenticate')->willThrowException($exception);
        $this->guard->expects($this->never())->method('setToken');

        $this->expectExceptionMessage('foo');
        $this->expectException(\RuntimeException::class);

        $this->throughFirewall();
    }

    protected function getFirewall()
    {
        return new AnonymousFirewall($this->guard, new AnonymousKey('foo'));
    }
}