<?php

declare(strict_types=1);

namespace ThrustbitTests\Security\Unit\Application\Http\Request\Firewall;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Application\Http\Request\Firewall\LogoutFirewall;
use Thrustbit\Security\Application\Http\Response\Logout\Contract\LogoutSuccess;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\TrustResolver;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;
use Thrustbit\Security\Infrastructure\Services\Logout\Contract\Logout;

class LogoutFirewallTest extends FirewallTestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|LogoutSuccess
     */
    private $logoutResponse;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|TrustResolver
     */
    private $trustResolver;

    public function setUp()
    {
        parent::setUp();

        $this->logoutResponse = $this->getMockForAbstractClass(LogoutSuccess::class);
        $this->trustResolver = $this->getMockForAbstractClass(TrustResolver::class);
    }

    /**
     * @test
     */
    public function it_determine_if_authentication_is_required_with_token_storage(): void
    {
        $this->guard->expects($this->once())->method('isStorageEmpty')->willReturn(true);
        $this->trustResolver->expects($this->never())->method('isAnonymous');
        $this->authenticationRequest->expects($this->never())->method('matches');

        $this->assertNull($this->throughFirewall());
    }

    /**
     * @test
     */
    public function it_determine_if_authentication_is_required_with_token_type(): void
    {
        $this->guard->expects($this->once())->method('isStorageEmpty')->willReturn(false);
        $token = $this->getMockForAbstractClass(Tokenable::class);
        $this->guard->expects($this->once())->method('getToken')->willReturn($token);
        $this->trustResolver->expects($this->once())->method('isAnonymous')->willReturn(true);
        $this->authenticationRequest->expects($this->never())->method('matches');

        $this->assertNull($this->throughFirewall());
    }

    /**
     * @test
     */
    public function it_determine_if_authentication_is_required_with_authentication_request(): void
    {
        $this->authenticationRequired();
        $this->authenticationRequest->expects($this->once())->method('matches')->willReturn(false);

        $this->assertNull($this->throughFirewall());
    }

    /**
     * @test
     */
    public function it_add_logout_handler(): void
    {
        $firewall = $this->getFirewall();
        $class = new \ReflectionClass($firewall);
        $handlers = $class->getProperty('logoutHandlers');
        $handlers->setAccessible(true);

        $this->assertCount(0, $handlers->getValue($firewall));

        $logoutHandler = $this->getMockForAbstractClass(Logout::class);
        $firewall->addHandler($logoutHandler);

        $this->assertCount(1, $handlers->getValue($firewall));
    }

    /**
     * @test
     */
    public function it_return_response_and_logout_all_handlers(): void
    {
        $this->authenticationRequired();
        $this->authenticationRequest->expects($this->once())->method('matches')->willReturn(true);

        $mock = new Response('foo');
        $this->logoutResponse->expects($this->once())->method('onLogoutSuccess')->willReturn($mock);

        $this->guard->expects($this->once())->method('clearStorage');
        $this->guard->expects($this->once())->method('dispatchLogoutEvent');

        $this->assertSame($mock, $this->throughLogoutFirewall());
    }

    protected function getFirewall(): LogoutFirewall
    {
        return new LogoutFirewall(
            $this->guard,
            $this->logoutResponse,
            $this->trustResolver,
            $this->authenticationRequest
        );
    }

    protected function throughLogoutFirewall(): ?Response
    {
        $logoutHandler = $this->getMockForAbstractClass(Logout::class);
        $logoutHandler->expects($this->once())->method('logout');

        $logoutHandler1 = $this->getMockForAbstractClass(Logout::class);
        $logoutHandler1->expects($this->once())->method('logout');

        $firewall = $this->getFirewall();
        $firewall->addHandler($logoutHandler);
        $firewall->addHandler($logoutHandler1);

        $request = $this->getMockForAbstractClass(Request::class);

        return $firewall->handle($request, function () {
        });
    }

    protected function authenticationRequired(): void
    {
        $this->guard->expects($this->once())->method('isStorageEmpty')->willReturn(false);
        $token = $this->getMockForAbstractClass(Tokenable::class);
        $this->guard->expects($this->atLeastOnce())->method('getToken')->willReturn($token);
        $this->trustResolver->expects($this->once())->method('isAnonymous')->willReturn(false);
    }
}