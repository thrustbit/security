<?php

declare(strict_types=1);

namespace ThrustbitTests\Security\Unit\Application\Http\Request\Firewall;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Thrustbit\Security\Application\Exceptions\Service\InvalidArgument;
use Thrustbit\Security\Application\Exceptions\Service\UnsupportedUser;
use Thrustbit\Security\Application\Http\Request\Firewall\ContextControlFirewall;
use Thrustbit\Security\Application\Values\SecurityKey\ContextKey;
use Thrustbit\Security\Application\Values\SecurityKey\ProviderKey;
use Thrustbit\Security\Domain\User\Exceptions\UserNotFound;
use Thrustbit\Security\Domain\User\Provider\UserProvider;
use Thrustbit\Security\Domain\User\Values\BcryptPassword;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Storage;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Storage\TokenStorage;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\UserNamePasswordToken;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Values\SimpleCredentialFactory;
use ThrustbitTests\Security\Mock\SimpleUserSecurity;
use ThrustbitTests\Security\Mock\SomeUserIdentifier;
use ThrustbitTests\Security\TestCase;

class ContextControlFirewallTest extends TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Request
     */
    private $request;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Dispatcher
     */
    private $dispatcher;

    /**
     * @var ContextKey
     */
    private $contextKey;

    /**
     * @var Storage
     */
    private $storage;

    /**
     * @var UserNamePasswordToken
     */
    private $token;

    /**
     * @var SimpleUserSecurity
     */
    private $user;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|SessionInterface
     */
    private $session;

    public function setUp()
    {
        $this->request = $this->getMockBuilder(Request::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        // expects "any" for dispatcher and session for empty user providers array test
        $this->session = $this->getMockForAbstractClass(SessionInterface::class);
        $this->request->expects($this->any())->method('session')->willReturn($this->session);

        $this->dispatcher = $this->getMockForAbstractClass(Dispatcher::class);
        $this->dispatcher->expects($this->any())->method('dispatch');

        $this->contextKey = new ContextKey('foo');
        $this->storage = new TokenStorage();
    }

    /**
     * @test
     */
    public function it_raise_exception_when_no_user_provider_provided(): void
    {
        $this->expectException('Provide at least one user provider for context firewall');
        $this->expectException(InvalidArgument::class);

        $request = $this->getMockForAbstractClass(Request::class);

        $this->getFirewall()->handle($request, function () {
        });
    }

    /**
     * @test
     */
    public function it_return_null_when_no_token_found_in_request_session(): void
    {
        $this->session->expects($this->once())->method('get')->willReturn(null);

        $userProvider = $this->getMockForAbstractClass(UserProvider::class);
        $userProvider->expects($this->never())->method('refreshUser');

        $response = $this->getFirewall([$userProvider])->handle($this->request, function () {
        });
        $this->assertNull($response);
        $this->assertNull($this->storage->getToken());
    }

    /**
     * @test
     */
    public function it_return_null_when_unserialize_does_not_implement_tokenable_interface(): void
    {
        $this->session->expects($this->once())->method('get')->willReturn(serialize('foo'));

        $userProvider = $this->getMockForAbstractClass(UserProvider::class);
        $userProvider->expects($this->never())->method('refreshUser');

        $response = $this->getFirewall([$userProvider])->handle($this->request, function () {
        });
        $this->assertNull($response);
        $this->assertNull($this->storage->getToken());
    }

    /**
     * @test
     */
    public function it_refresh_user_on_token(): void
    {
        $this->assertNull($this->storage->getToken());

        $this->session->expects($this->once())->method('get')
            ->willReturn($this->getTokenStringForSession());

        $userProvider = $this->getMockForAbstractClass(UserProvider::class);
        $userProvider->expects($this->once())->method('supportsClass')->willReturn(true);
        $userProvider->expects($this->once())->method('refreshUser')->willReturn($this->user);

        $response = $this->getFirewall([$userProvider])->handle($this->request, function () {
        });

        $this->assertSame($this->token->getUser(), $this->user); // ??
        $this->assertEquals($this->token, $this->storage->getToken());
        $this->assertNull($response);
    }

    /**
     * @test
     */
    public function it_does_not_refresh_user_when_user_from_token_does_not_implement_user_security_interface(): void
    {
        $user = new SomeUserIdentifier(true);
        $token = new UserNamePasswordToken($user, SimpleCredentialFactory::fromString('foo_bar_foo'), new ProviderKey('foo'));

        $this->assertNull($this->storage->getToken());
        $this->session->expects($this->once())->method('get')->willReturn(serialize($token));

        $userProvider = $this->getMockForAbstractClass(UserProvider::class);
        $userProvider->expects($this->never())->method('supportsClass');

        $response = $this->getFirewall([$userProvider])->handle($this->request, function () {
        });

        $this->assertInstanceOf(UserNamePasswordToken::class, $this->storage->getToken());
        $this->assertNull($response);
    }

    /**
     * @test
     */
    public function it_return_null_when_user_provider_raise_user_not_found_exception()
    {
      //  $this->markTestSkipped('Unserialize bug');

        $this->assertNull($this->storage->getToken());

        $this->session->expects($this->once())->method('get')
            ->willReturn($this->getTokenStringForSession());

        $userProvider = $this->getMockForAbstractClass(UserProvider::class);
        $userProvider->expects($this->once())->method('supportsClass')->willReturn(true);

        $exception = new UserNotFound('foo');
        $userProvider->expects($this->once())->method('refreshUser')->willThrowException($exception);

        $response = $this->getFirewall([$userProvider])->handle($this->request, function () {
        });

        $this->assertNull($response);
        $this->assertNull($this->storage->getToken());
    }

    /**
     * @test
     */
    public function it_iterate_over_user_providers_till_first_success_result(): void
    {
       // $this->markTestSkipped('Unserialize bug');

        $this->assertNull($this->storage->getToken());

        $this->session->expects($this->once())->method('get')
            ->willReturn($this->getTokenStringForSession());

        $userProvider = $this->getMockForAbstractClass(UserProvider::class);
        $userProvider->expects($this->once())->method('supportsClass')->willReturn(false);
        $userProvider->expects($this->never())->method('refreshUser');

        $userProvider1 = $this->getMockForAbstractClass(UserProvider::class);
        $userProvider1->expects($this->once())->method('supportsClass')->willReturn(true);
        $userProvider1->expects($this->once())->method('refreshUser')->willReturn($this->user);

        $response = $this->getFirewall([$userProvider, $userProvider1])->handle($this->request, function () {
        });

        $this->assertNull($response);
        $this->assertInstanceOf(UserNamePasswordToken::class, $this->token);
        $this->assertSame($this->user, $this->storage->getToken()->getUser());
    }

    /**
     * @test
     */
    public function it_raise_exception_when_no_user_provider_supports_user()
    {
       // $this->markTestSkipped('Unserialize bug');

        $this->assertNull($this->storage->getToken());

        $this->session->expects($this->once())->method('get')
            ->willReturn($this->getTokenStringForSession());

        $userProvider = $this->getMockForAbstractClass(UserProvider::class);
        $userProvider->expects($this->once())->method('supportsClass')->willReturn(false);
        $userProvider->expects($this->never())->method('refreshUser');

        $this->expectException(UnsupportedUser::class);

        $this->getFirewall([$userProvider])->handle($this->request, function () {
        });
    }

    private function getFirewall(array $userProviders = []): ContextControlFirewall
    {
        return new ContextControlFirewall(
            $this->storage, $this->contextKey, $this->dispatcher, $userProviders
        );
    }

    private function getTokenStringForSession(): string
    {
        // fixMe call $this->>user->getPassword() will result
        // in an unserialize error when pass to context firewall

        // this dumb fix will produce error on userHasChanged implementation on token trait

        $this->token = new UserNamePasswordToken(
            $this->user = new SimpleUserSecurity(),
            new BcryptPassword(password_hash('password', PASSWORD_BCRYPT)),
            new ProviderKey('foo')
        );

        return serialize($this->token);
    }
}