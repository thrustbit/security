<?php

declare(strict_types=1);

namespace ThrustbitTests\Security\Unit\Application\Http\Request\Firewall;

use Illuminate\Http\Request;
use Thrustbit\Security\Application\Exceptions\AuthenticationException;
use Thrustbit\Security\Application\Http\Request\Firewall\SimplePreAuthenticationFirewall;
use Thrustbit\Security\Application\Http\Response\Authentication\Contract\AuthenticationFailure;
use Thrustbit\Security\Application\Http\Response\Authentication\Contract\AuthenticationSuccess;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\SimplePreAuthenticator;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;
use ThrustbitTests\Security\Mock\PreAuthenticatorWithResponse;

class SimplePreAuthenticationFirewallTest extends FirewallTestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|SimplePreAuthenticator
     */
    private $authenticator;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Tokenable
     */
    private $token;

    public function setUp()
    {
        parent::setUp();

        $this->authenticator = $this->getMockForAbstractClass(SimplePreAuthenticator::class);
        $this->token = $this->getMockForAbstractClass(Tokenable::class);
    }

    /**
     * @test
     */
    public function it_determine_if_authentication_is_required_with_token_storage(): void
    {
        $this->guard->expects($this->once())->method('isStorageEmpty')->willReturn(false);
        $this->authenticationRequest->expects($this->never())->method('matches');

        $this->authenticator->expects($this->never())->method('createToken');
        $this->assertNull($this->throughFirewall());
    }

    /**
     * @test
     */
    public function it_determine_if_authentication_is_required_with_authentication_request(): void
    {
        $this->configureProcessingAuthentication();

        $this->assertNull($this->throughFirewall());
    }

    /**
     * @test
     */
    public function it_return_null_response_on_authentication_success_and_authenticator_response_free(): void
    {
        $this->configureProcessingAuthentication();

        $this->assertNotInstanceOf(AuthenticationSuccess::class, $this->authenticator);
        $this->authenticator->expects($this->once())->method('createToken')->willReturn($this->token);

        $this->guard->expects($this->once())->method('authenticate')->willReturn($this->token);

        $this->assertFalse($this->stateless);
        $this->guard->expects($this->once())->method('dispatchLoginEvent');
        $this->guard->expects($this->once())->method('setToken');

        $this->assertNull($this->throughFirewall());
    }

    /**
     * @test
     */
    public function it_return_response_on_authentication_success_and_authenticator_implement_authentication_success(): void
    {
        $this->configureProcessingAuthentication();

        $authenticator = new PreAuthenticatorWithResponse();
        $this->assertInstanceOf(AuthenticationSuccess::class, $authenticator);

        $request = $this->getMockForAbstractClass(Request::class);
        $token = $authenticator->createToken($request, $this->providerKey);

        $this->guard->expects($this->once())->method('authenticate')->willReturn($token);

        $this->assertFalse($this->stateless);
        $this->guard->expects($this->once())->method('dispatchLoginEvent');
        $this->guard->expects($this->never())->method('setToken');


        $firewall = new SimplePreAuthenticationFirewall(
            $this->guard, $authenticator, $this->authenticationRequest, $this->providerKey, $this->stateless
        );

        $response = $firewall->handle($request, function () {
        });
        $authResponse = $authenticator->onAuthenticationSuccess($request, $token);

        $this->assertEquals($authResponse, $response);
        $this->assertSame((string)$authResponse, (string)$response);
    }

    /**
     * @test
     */
    public function it_return_response_on_authentication_failure_and_authenticator_implement_authentication_failure(): void
    {
        $this->configureProcessingAuthentication();

        $authenticator = new PreAuthenticatorWithResponse(true, true);
        $this->assertInstanceOf(AuthenticationFailure::class, $authenticator);

        $request = $this->getMockForAbstractClass(Request::class);

        // authenticator should be proxy by the authentication manager to authenticate the created token
        // we only here throw exception from guard to mock it

        $exception = new AuthenticationException('some message');
        $this->guard->expects($this->once())->method('authenticate')->willThrowException($exception);

        $firewall = new SimplePreAuthenticationFirewall(
            $this->guard, $authenticator, $this->authenticationRequest, $this->providerKey, $this->stateless
        );

        $response = $firewall->handle($request, function () {
        });
        $authResponse = $authenticator->onAuthenticationFailure($request);

        $this->assertEquals($authResponse, $response);
        $this->assertSame((string)$authResponse, (string)$response);
    }

    protected function configureProcessingAuthentication(): void
    {
        $this->guard->expects($this->once())->method('isStorageEmpty')->willReturn(true);
        $this->authenticationRequest->expects($this->once())->method('matches')->willReturn(true);
    }

    protected function getFirewall()
    {
        return new SimplePreAuthenticationFirewall(
            $this->guard,
            $this->authenticator,
            $this->authenticationRequest,
            $this->providerKey,
            $this->stateless
        );
    }
}