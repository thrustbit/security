<?php

declare(strict_types=1);

namespace ThrustbitTests\Security\Unit\Application\Http\Request\Firewall;

use Thrustbit\Security\Application\Exceptions\AuthenticationException;
use Thrustbit\Security\Application\Http\Request\Firewall\RecallerFirewall;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;
use Thrustbit\Security\Infrastructure\Services\Recaller\Contract\Recallable;

class RecallerFirewallTest extends FirewallTestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Recallable
     */
    private $recallable;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Tokenable
     */
    private $token;

    public function setUp()
    {
        parent::setUp();

        $this->recallable = $this->getMockForAbstractClass(Recallable::class);
        $this->token = $this->getMockForAbstractClass(Tokenable::class);
    }

    /**
     * @test
     */
    public function it_determine_if_authentication_is_required(): void
    {
        $this->guard->expects($this->once())->method('isStorageEmpty')->willReturn(false);
        $this->recallable->expects($this->never())->method('autoLogin');
        $this->guard->expects($this->never())->method('authenticate');

        $this->assertNull($this->throughFirewall());
    }

    /**
     * @test
     */
    public function it_return_null_response_when_recaller_does_not_exist_on_request(): void
    {
        $this->guard->expects($this->once())->method('isStorageEmpty')->willReturn(true);
        $this->recallable->expects($this->once())->method('autoLogin')->willReturn(null);
        $this->guard->expects($this->never())->method('authenticate');

        $this->assertNull($this->throughFirewall());
    }

    /**
     * @test
     */
    public function it_return_null_response_on_authentication_success(): void
    {
        $this->guard->expects($this->once())->method('isStorageEmpty')->willReturn(true);
        $this->recallable->expects($this->once())->method('autoLogin')->willReturn($this->token);

        $this->guard->expects($this->once())->method('authenticate')->willReturn($this->token);
        $this->guard->expects($this->once())->method('setToken');
        $this->guard->expects($this->once())->method('dispatchLoginEvent');

        $this->assertNull($this->throughFirewall());
    }

    /**
     * @test
     */
    public function it_raise_authentication_exception_on_authentication_failure(): void
    {
        $this->guard->expects($this->once())->method('isStorageEmpty')->willReturn(true);
        $this->recallable->expects($this->once())->method('autoLogin')->willReturn($this->token);

        $exception = new AuthenticationException('foo');
        $this->guard->expects($this->once())->method('authenticate')->willThrowException($exception);

        $this->recallable->expects($this->once())->method('loginFail');

        $this->expectExceptionMessage('foo');
        $this->expectException(AuthenticationException::class);

        $this->throughFirewall();
    }

    protected function getFirewall()
    {
        return new RecallerFirewall($this->recallable, $this->guard);
    }
}