<?php

declare(strict_types=1);

namespace ThrustbitTests\Security\Unit\Application\Http\Request\Firewall;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Application\Http\Request\Firewall\Contract\AuthenticationRequest;
use Thrustbit\Security\Application\Http\Request\Firewall\Contract\AuthenticationResponse;
use Thrustbit\Security\Application\Values\SecurityKey\ProviderKey;
use Thrustbit\Security\Infrastructure\Guard\Guard;
use ThrustbitTests\Security\TestCase;

abstract class FirewallTestCase extends TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Guard
     */
    protected $guard;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|AuthenticationRequest
     */
    protected $authenticationRequest;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|AuthenticationResponse
     */
    protected $authenticationResponse;

    /**
     * @var ProviderKey
     */
    protected $providerKey;

    /**
     * @var bool
     */
    protected $stateless;

    public function setUp()
    {
        parent::setUp();

        $this->guard = $this->getMockBuilder(Guard::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $this->authenticationResponse = $this->getMockForAbstractClass(AuthenticationResponse::class);
        $this->authenticationRequest = $this->getMockForAbstractClass(AuthenticationRequest::class);
        $this->providerKey = new ProviderKey('foo');
        $this->stateless = false;
    }

    protected function throughFirewall(): ?Response
    {
        $request = $this->getMockForAbstractClass(Request::class);

        return $this->getFirewall()->handle($request, function () {
        });
    }

    abstract protected function getFirewall();
}