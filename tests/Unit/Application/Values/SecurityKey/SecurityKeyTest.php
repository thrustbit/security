<?php

declare(strict_types=1);

namespace ThrustbitTests\Security\Unit\Application\Values\SecurityKey;

use Thrustbit\Security\Application\Values\SecurityKey;
use ThrustbitTests\Security\Mock\SomeSecurityKey;
use ThrustbitTests\Security\TestCase;

class SecurityKeyTest extends TestCase
{

    /**
     * @test
     */
    public function it_can_be_constructed(): void
    {
        $key = new SomeSecurityKey('foo');
        $this->assertInstanceOf(SecurityKey::class, $key);
    }

    /**
     * @test
     * @expectedException \Thrustbit\Security\Application\Exceptions\SecurityValueFailed
     * @dataProvider provideNotValidKey
     */
    public function it_raise_exception_when_key_is_not_valid($key)
    {
        new SomeSecurityKey($key);
    }

    /**
     * @test
     */
    public function it_can_be_compared(): void
    {
        $key = new SomeSecurityKey('foo');
        $this->assertTrue($key->sameValueAs($key));

        $aKey = new SomeSecurityKey('bar');
        $this->assertFalse($aKey->sameValueAs($key));
    }

    /**
     * @test
     */
    public function it_can_be_serialized(): void
    {
        $key = new SomeSecurityKey('foo');

        $this->assertSame('foo', $key->getKey());
        $this->assertSame('foo', (string)$key);
    }

    public function provideNotValidKey()
    {
        return [[null, '', 1234, []]];
    }
}