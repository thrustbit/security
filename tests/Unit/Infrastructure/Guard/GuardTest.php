<?php

declare(strict_types=1);

namespace ThrustbitTests\Security\Unit\Infrastructure\Guard;

use Illuminate\Http\Request;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;
use Illuminate\Contracts\Events\Dispatcher;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\Authenticatable;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Storage;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Storage\TokenStorage;
use Thrustbit\Security\Infrastructure\Guard\Guard;
use Thrustbit\Security\Infrastructure\Guard\SecurityEventDispatcher;
use ThrustbitTests\Security\TestCase;

class GuardTest extends TestCase
{
    public function setUp()
    {
        $this->setUpGuard();
    }

    /**
     * @test
     */
    public function it_has_access_to_token_storage(): void
    {
        $this->storage->setToken($this->token);

        $this->assertSame($this->token, $this->guard->getToken());
    }

    /**
     * @test
     */
    public function it_clear_token_storage(): void
    {
        $this->assertTrue($this->guard->isStorageEmpty());

        $this->storage->setToken($this->token);

        $this->assertSame($this->token, $this->guard->getToken());

        $this->guard->clearStorage();

        $this->assertTrue($this->guard->isStorageEmpty());
        $this->assertFalse($this->guard->isStorageNotEmpty());
        $this->assertNull($this->guard->getToken());
    }

    /**
     * @test
     */
    public function it_return_same_token_on_authentication(): void
    {
        $this->authenticationManager->expects($this->once())->method('authenticate')->willReturn($this->token);

        $this->assertSame($this->token, $this->guard->authenticate($this->token));
    }

    /**
     * @test
     */
    public function it_return_different_token_on_authentication(): void
    {
        $authenticatedToken = $this->getMockForAbstractClass(Tokenable::class);

        $this->authenticationManager->expects(
            $this->once())->method('authenticate')->willReturn($authenticatedToken);

        $this->assertNotSame($this->token, $this->guard->authenticate($this->token));
    }

    /**
     * @test
     */
    public function it_dispatch_logout_authentication_event(): void
    {
        $this->eventDispatcher->expects($this->once())->method('dispatch');

        $this->guard->dispatchLogoutEvent($this->token);
    }

    /**
     * @test
     */
    public function it_dispatch_login_authentication_event(): void
    {
        $this->eventDispatcher->expects($this->once())->method('dispatch');

        $this->guard->dispatchLoginEvent(
            $this->getMockForAbstractClass(Request::class),
            $this->token
        );
    }

    /**
     * @test
     * @expectedException \Thrustbit\Security\Application\Exceptions\Service\CredentialsNotFound
     */
    public function it_raise_exception_when_token_is_required_and_token_storage_is_empty(): void
    {
        $this->assertTrue($this->guard->isStorageEmpty());

        $this->guard->requireToken();
    }

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Guard
     */
    protected $guard;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Tokenable
     */
    protected $token;

    /**
     * @var Storage
     */
    protected $storage;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Authenticatable
     */
    protected $authenticationManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Dispatcher
     */
    protected $eventDispatcher;

    /**
     * @var SecurityEventDispatcher
     */
    protected $securityDispatcher;

    public function setUpGuard()
    {
        $this->token = $this->getMockForAbstractClass(Tokenable::class);

        $this->storage = new TokenStorage();

        $this->authenticationManager = $this->getMockForAbstractClass(Authenticatable::class);

        $this->eventDispatcher = $this->getMockForAbstractClass(Dispatcher::class);

        $this->securityDispatcher = new SecurityEventDispatcher($this->eventDispatcher);

        $this->guard = new Guard($this->storage, $this->authenticationManager, $this->securityDispatcher);
    }
}