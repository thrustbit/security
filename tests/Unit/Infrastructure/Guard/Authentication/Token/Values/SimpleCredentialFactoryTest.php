<?php

declare(strict_types=1);

namespace ThrustbitTests\Security\Unit\Infrastructure\Guard\Authentication\Token\Values;

use Thrustbit\Security\Domain\User\Values\ClearPassword;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Values\EmptyCredential;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Values\SimpleCredentialFactory;
use ThrustbitTests\Security\TestCase;

class SimpleCredentialFactoryTest extends TestCase
{

    /**
     * @test
     *
     * @param $credentials
     * @dataProvider provideEmptyCredentials
     */
    public function it_return_empty_credentials_instance($credentials): void
    {
        $password = SimpleCredentialFactory::fromString($credentials);

        $this->assertInstanceOf(EmptyCredential::class, $password);
    }

    /**
     * @test
     */
    public function it_return_clear_password_instance(): void
    {
        $password = SimpleCredentialFactory::fromString('password');

        $this->assertInstanceOf(ClearPassword::class, $password);
    }

    public function provideEmptyCredentials(): array
    {
        return [
            [''],
            [null],
            [' '],
            ['                       ']
        ];
    }
}