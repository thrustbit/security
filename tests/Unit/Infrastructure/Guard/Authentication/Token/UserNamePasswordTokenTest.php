<?php

declare(strict_types=1);

namespace ThrustbitTests\Security\Unit\Infrastructure\Guard\Authentication\Token;

use Thrustbit\Security\Application\Values\SecurityKey\ProviderKey;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\UserNamePasswordToken;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Values\SimpleCredentialFactory;
use ThrustbitTests\Security\Mock\SimpleUserSecurity;
use ThrustbitTests\Security\TestCase;

class UserNamePasswordTokenTest extends TestCase
{
    /**
     * @test
     */
    public function it_can_be_serialize_and_unserialize(): void
    {
        $token = $this->getToken();

        $str = serialize($token);
        $returnValue = unserialize($str);

        $this->assertEquals($token, $returnValue);
    }

    private function getToken(): UserNamePasswordToken
    {
        return new UserNamePasswordToken(
            new SimpleUserSecurity(),
            SimpleCredentialFactory::fromString('foo_bar_foo'),
            new ProviderKey('foo')
        );
    }
}