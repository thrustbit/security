<?php

declare(strict_types=1);

namespace ThrustbitTests\Security\Unit\Infrastructure\Guard\Authentication;

use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\TrustResolver;
use Thrustbit\Security\Infrastructure\Guard\Authentication\DefaultTrustResolver;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\AnonymousToken;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\RecallerToken;
use ThrustbitTests\Security\TestCase;

class DefaultTrustResolverTest extends TestCase
{
    /**
     * @test
     */
    public function it_assert_token_is_not_anonymous_nor_remember()
    {
        $trust = $this->getTrustResolver();

        $this->assertFalse($trust->isFullFledged());

        $token = $this->getMockForAbstractClass(Tokenable::class);
        $this->assertTrue($trust->isFullFledged($token));
    }

    /**
     * @test
     */
    public function it_assert_token_is_anonymous(): void
    {
        $trust = $this->getTrustResolver();

        $anon = $this->getMockBuilder(AnonymousToken::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->assertFalse($trust->isAnonymous());
        $this->assertTrue($trust->isAnonymous($anon));
    }

    /**
     * @test
     */
    public function it_assert_token_is_remember(): void
    {
        $trust = $this->getTrustResolver();

        $recaller = $this->getMockBuilder(RecallerToken::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->assertFalse($trust->isRememberMe());
        $this->assertTrue($trust->isRememberMe($recaller));
    }

    protected function getTrustResolver(): TrustResolver
    {
        return new DefaultTrustResolver(AnonymousToken::class, RecallerToken::class);
    }
}
