<?php

declare(strict_types=1);

namespace ThrustbitTests\Security\Unit\Infrastructure\Guard\Authentication;

use Thrustbit\Security\Infrastructure\Guard\Authentication\AuthenticationManager;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\AuthenticationProvider;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;
use ThrustbitTests\Security\TestCase;

class AuthenticationManagerTest extends TestCase
{
    /**
     * @test
     */
    public function it_authenticate_token(): void
    {
        $userProviders = [];
        $userProviders [] = $userProvider = $this->getMockForAbstractClass(AuthenticationProvider::class);

        $token = $this->getMockForAbstractClass(Tokenable::class);
        $userProvider->expects($this->once())->method('supports')->willReturn(true);
        $userProvider->expects($this->once())->method('authenticate')->willReturn($token);

        $manager = new AuthenticationManager($userProviders);
        $authenticatedToken = $manager->authenticate($token);

        $this->assertSame($authenticatedToken, $token);
    }

    /**
     * @test
     */
    public function it_return_first_supported_authentication_provider(): void
    {
        $userProvider = $this->getMockForAbstractClass(AuthenticationProvider::class);
        $userProvider2 = $this->getMockForAbstractClass(AuthenticationProvider::class);

        $userProviders = [$userProvider, $userProvider2];

        $token = $this->getMockForAbstractClass(Tokenable::class);
        $userProvider->expects($this->once())->method('supports')->willReturn(true);
        $userProvider->expects($this->once())->method('authenticate')->willReturn($token);

        $userProvider2->expects($this->never())->method('supports');
        $userProvider2->expects($this->never())->method('authenticate');

        $manager = new AuthenticationManager($userProviders);
        $authenticatedToken = $manager->authenticate($token);

        $this->assertSame($authenticatedToken, $token);
    }

    /**
     * @test
     * @expectedException \Thrustbit\Security\Application\Exceptions\Service\UnsupportedProvider
     */
    public function it_raise_exception_when_no_authentication_provider_supports_token(): void
    {
        $userProviders = [$userProvider = $this->getMockForAbstractClass(AuthenticationProvider::class)];

        $userProvider->expects($this->once())->method('supports')->willReturn(false);
        $userProvider->expects($this->never())->method('authenticate');
        $token = $this->getMockForAbstractClass(Tokenable::class);

        $manager = new AuthenticationManager($userProviders);
        $manager->authenticate($token);
    }

    /**
     * @test
     * @expectedException \Thrustbit\Security\Application\Exceptions\Service\UnsupportedProvider
     */
    public function it_raise_exception_when_no_authentication_provider_provided(): void
    {
        $token = $this->getMockForAbstractClass(Tokenable::class);

        $manager = new AuthenticationManager([]);
        $manager->authenticate($token);
    }
}