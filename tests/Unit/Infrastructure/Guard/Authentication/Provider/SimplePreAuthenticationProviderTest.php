<?php

declare(strict_types=1);

namespace ThrustbitTests\Security\Unit\Infrastructure\Guard\Authentication\Provider;

use Thrustbit\Security\Application\Values\SecurityKey\ProviderKey;
use Thrustbit\Security\Domain\User\Provider\UserProvider;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\SimplePreAuthenticator;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Provider\SimplePreAuthenticationProvider;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\PreAuthenticatedToken;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Values\SimpleCredentialFactory;
use ThrustbitTests\Security\Mock\SomeUserIdentifier;
use ThrustbitTests\Security\TestCase;

class SimplePreAuthenticationProviderTest extends TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|SimplePreAuthenticator
     */
    private $authenticator;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|UserProvider
     */
    private $userProvider;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Tokenable
     */
    private $token;

    /**
     * @var ProviderKey
     */
    private $providerKey;

    public function setUp()
    {
        $this->authenticator = $this->getMockForAbstractClass(SimplePreAuthenticator::class);
        $this->userProvider = $this->getMockForAbstractClass(UserProvider::class);
        $this->token = $this->getMockForAbstractClass(Tokenable::class);
        $this->providerKey = new ProviderKey('foo');
    }

    /**
     * @test
     */
    public function it_determine_if_token_is_supported_by_authentication_provider(): void
    {
        $auth = $this->getProvider();
        $this->authenticator->expects($this->once())->method('supportsToken')->willReturn(false);
        $this->authenticator->expects($this->never())->method('authenticateToken');

        $auth->supports($this->token);
    }

    /**
     * @test
     */
    public function it_proxy_authentication_to_authenticator(): void
    {
        $auth = $this->getProvider();

        $authenticatedToken = new PreAuthenticatedToken(
            new SomeUserIdentifier(true), SimpleCredentialFactory::fromString('foo_bar_foo'), $this->providerKey
        );

        $this->authenticator->expects($this->once())->method('authenticateToken')
            ->willReturn($authenticatedToken);

        $tokenFromProxy = $auth->authenticate($this->token);

        $this->assertNotEquals($this->token, $tokenFromProxy);
        $this->assertEquals($authenticatedToken, $tokenFromProxy);
    }


    public function getProvider(): SimplePreAuthenticationProvider
    {
        return new SimplePreAuthenticationProvider($this->authenticator, $this->userProvider, $this->providerKey);
    }
}