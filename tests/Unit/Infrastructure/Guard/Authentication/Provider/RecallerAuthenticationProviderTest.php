<?php

declare(strict_types=1);

namespace ThrustbitTests\Security\Unit\Infrastructure\Guard\Authentication\Provider;

use Thrustbit\Security\Application\Values\SecurityKey\ProviderKey;
use Thrustbit\Security\Application\Values\SecurityKey\RecallerKey;
use Thrustbit\Security\Domain\User\Exceptions\UserStatusException;
use Thrustbit\Security\Domain\User\Services\SecurityUserChecker;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Provider\RecallerAuthenticationProvider;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\RecallerToken;
use ThrustbitTests\Security\Mock\SimpleUserSecurity;
use ThrustbitTests\Security\TestCase;

class RecallerAuthenticationProviderTest extends TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|SecurityUserChecker
     */
    private $userChecker;

    /**
     * @var ProviderKey
     */
    private $providerKey;

    /**
     * @var RecallerKey
     */
    private $recallerKey;

    public function setUp()
    {
        $this->userChecker = $this->getMockForAbstractClass(SecurityUserChecker::class);
        $this->providerKey = new ProviderKey('foo');
        $this->recallerKey = new RecallerKey('bar');
    }

    /**
     * @test
     * @expectedException \Thrustbit\Security\Application\Exceptions\Service\UnsupportedProvider
     */
    public function it_raise_exception_when_token_is_not_supported_by_authentication_provider(): void
    {
        $auth = $this->getProvider();

        $token = new RecallerToken(
            new SimpleUserSecurity(), new ProviderKey('foo_foo'), new RecallerKey('foo_bar')
        );
        $this->userChecker->expects($this->never())->method('onPreAuthentication');

        $auth->authenticate($token);
    }

    /**
     * @test
     * @expectedException \Thrustbit\Security\Domain\User\Exceptions\BadCredentials
     */
    public function it_raise_exception_when_recaller_key_does_not_match(): void
    {
        $auth = $this->getProvider();

        $token = new RecallerToken(
            new SimpleUserSecurity(), $this->providerKey, new RecallerKey('foo_bar')
        );
        $this->userChecker->expects($this->never())->method('onPreAuthentication');

        $auth->authenticate($token);
    }

    /**
     * @test
     */
    public function it_return_token_on_authentication(): void
    {
        $auth = $this->getProvider();

        $token = new RecallerToken(
            new SimpleUserSecurity(), $this->providerKey, $this->recallerKey
        );

        $this->userChecker->expects($this->once())->method('onPreAuthentication');

        $authenticatedToken = $auth->authenticate($token);

        $this->assertEquals($token, $authenticatedToken);
    }

    /**
     * @test
     * @expectedException  \Thrustbit\Security\Domain\User\Exceptions\UserStatusException
     */
    public function it_raise_user_status_exception(): void
    {
        $auth = $this->getProvider();

        $token = new RecallerToken(
            new SimpleUserSecurity(), $this->providerKey, $this->recallerKey
        );

        $exception = new UserStatusException('some message');
        $this->userChecker->expects($this->once())->method('onPreAuthentication')
            ->willThrowException($exception);

        $auth->authenticate($token);
    }

    private function getProvider(): RecallerAuthenticationProvider
    {
        return new RecallerAuthenticationProvider(
            $this->providerKey, $this->recallerKey, $this->userChecker
        );
    }
}