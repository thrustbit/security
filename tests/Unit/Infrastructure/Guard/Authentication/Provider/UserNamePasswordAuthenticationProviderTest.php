<?php

declare(strict_types=1);

namespace ThrustbitTests\Security\Unit\Infrastructure\Guard\Authentication\Provider;

use Illuminate\Contracts\Hashing\Hasher;
use Thrustbit\Security\Application\Values\EncodedPassword;
use Thrustbit\Security\Application\Values\SecurityKey\ProviderKey;
use Thrustbit\Security\Domain\Role\Values\SwitchUserRole;
use Thrustbit\Security\Domain\User\Exceptions\UserStatusException;
use Thrustbit\Security\Domain\User\LocalUser;
use Thrustbit\Security\Domain\User\Provider\UserProvider;
use Thrustbit\Security\Domain\User\Services\SecurityUserChecker;
use Thrustbit\Security\Domain\User\Values\EmailAddress;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Provider\UserNamePasswordAuthenticationProvider;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\UserNamePasswordToken;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Values\EmptyCredential;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Values\SimpleCredentialFactory;
use ThrustbitTests\Security\Mock\SomeUserIdentifier;
use ThrustbitTests\Security\TestCase;

class UserNamePasswordAuthenticationProviderTest extends TestCase
{
    public function setUp()
    {
        $this->setUpMock();
    }

    /**
     * @test
     * @expectedException \Thrustbit\Security\Application\Exceptions\Service\UnsupportedProvider
     */
    public function it_raise_exception_when_token_is_not_supported_by_authentication_provider(): void
    {
        $auth = $this->getProvider();

        $auth->authenticate($this->token);
    }

    /**
     * @test
     */
    public function it_assert_token_is_supported_by_authentication_provider(): void
    {
        $auth = $this->getProvider();
        $token = $this->getUserNamePasswordToken();

        $this->assertTrue($auth->supports($token));
    }

    /**
     * @test
     */
    public function it_assert_token_is_not_supported_with_different_provider_key(): void
    {
        $auth = $this->getProvider();
        $aToken = $this->getUserNamePasswordToken('bar');

        $this->assertFalse($auth->supports($aToken));
    }

    /**
     * @test
     */
    public function it_assert_token_is_not_supported_with_wrong_implementation(): void
    {
        $auth = $this->getProvider();

        $this->assertFalse($auth->supports($this->token));
    }

    /**
     * @test
     */
    public function it_does_not_require_user_provider_if_user_implement_local_user_interface(): void
    {
        $this->userProvider->expects($this->never())->method('requireByIdentifier');
        $this->encoder->expects($this->never())->method('check');

        $encodedCredential = $this->getMockForAbstractClass(EncodedPassword::class);
        $encodedCredential->expects($this->once())->method('sameValueAs')
            ->willReturn(true);

        $user = $this->getMockForAbstractClass(LocalUser::class);
        $user->expects($this->atLeastOnce())->method('getPassword')
            ->willReturn($encodedCredential);

        $token = new UserNamePasswordToken(
            $user,
            SimpleCredentialFactory::fromString('password'),
            $this->providerKey
        );

        $auth = $this->getProvider();
        $this->assertInstanceOf(UserNamePasswordToken::class, $auth->authenticate($token));
    }

    /**
     * @test
     */
    public function it_check_credential_provided_by_token_with_encoder(): void
    {
        $user = $this->getMockForAbstractClass(LocalUser::class);
        $this->userProvider->expects($this->once())->method('requireByIdentifier')->willReturn($user);
        $this->encoder->expects($this->once())->method('check')->willReturn(true);

        $token = $this->getFakeLoginToken();
        $auth = $this->getProvider();

        $this->assertInstanceOf(UserNamePasswordToken::class, $auth->authenticate($token));
    }

    /**
     * @test
     */
    public function it_return_authenticated_token_when_user_has_role(): void
    {
        $user = $this->getMockForAbstractClass(LocalUser::class);
        $this->userProvider->expects($this->once())->method('requireByIdentifier')->willReturn($user);
        $this->encoder->expects($this->once())->method('check')->willReturn(true);

        $token = $this->getFakeLoginToken();
        $auth = $this->getProvider();

        $user->expects($this->once())->method('getRoles')->willReturn(['ROLE_FOO']);

        $authenticatedToken = $auth->authenticate($token);

        $this->assertTrue($authenticatedToken->isAuthenticated());
    }

    /**
     * @test
     */
    public function it_return_non_authenticated_token_when_user_has_no_role(): void
    {
        $user = $this->getMockForAbstractClass(LocalUser::class);
        $user->expects($this->once())->method('getRoles')->willReturn([]);

        $this->userProvider->expects($this->once())->method('requireByIdentifier')->willReturn($user);
        $this->encoder->expects($this->once())->method('check')->willReturn(true);

        $token = $this->getFakeLoginToken();
        $auth = $this->getProvider();
        $returnToken = $auth->authenticate($token);

        $this->assertFalse($returnToken->isAuthenticated());
    }

    /**
     * @test
     */
    public function it_merge_dynamic_role_from_token_to_authenticated_user(): void
    {
        $user = $this->getMockForAbstractClass(LocalUser::class);
        $this->userProvider->expects($this->once())->method('requireByIdentifier')->willReturn($user);
        $this->encoder->expects($this->once())->method('check')->willReturn(true);

        $token = $this->getFakeLoginToken([new SwitchUserRole('SWITCH_USER_ROLE', $this->token)]);
        $this->assertCount(1, $token->getRoles());

        $user->expects($this->once())->method('getRoles')->willReturn(['ROLE_FOO']);

        $auth = $this->getProvider();
        $authenticatedToken = $auth->authenticate($token);

        $found = false;
        foreach ($authenticatedToken->getRoles() as $role) {
            if ($role instanceof SwitchUserRole) {
                $found = true;
            }
        }

        $this->assertTrue($found);
        $this->assertCount(2, $authenticatedToken->getRoles());
    }

    /**
     * @test
     * @expectedException \Thrustbit\Security\Domain\User\Exceptions\BadCredentials
     */
    public function it_raise_exception_on_empty_credential(): void
    {
        $user = $this->getMockForAbstractClass(LocalUser::class);
        $this->userProvider->expects($this->once())->method('requireByIdentifier')->willReturn($user);
        $this->encoder->expects($this->never())->method('check');

        $token = $this->getFakeLoginTokenWithEmptyCredential();

        $this->hideException = false;
        $this->getProvider()->authenticate($token);
    }

    /**
     * @test
     * @expectedException \Thrustbit\Security\Domain\User\Exceptions\BadCredentials
     */
    public function it_raise_exception_when_credential_does_not_match(): void
    {
        $user = $this->getMockForAbstractClass(LocalUser::class);
        $this->userProvider->expects($this->once())->method('requireByIdentifier')->willReturn($user);
        $this->encoder->expects($this->once())->method('check')->willReturn(false);

        $token = $this->getFakeLoginToken();

        $this->hideException = false;
        $this->getProvider()->authenticate($token);
    }

    /**
     * @test
     * @expectedException \Thrustbit\Security\Domain\User\Exceptions\UserStatusException
     */
    public function it_raise_exception_when_user_status_failed_on_pre_authentication(): void
    {
        $user = $this->getMockForAbstractClass(LocalUser::class);
        $this->userProvider->expects($this->once())->method('requireByIdentifier')->willReturn($user);
        $this->encoder->expects($this->never())->method('check');

        $token = $this->getFakeLoginToken();

        $this->userChecker->expects($this->once())->method('onPreAuthentication')
            ->willThrowException(new UserStatusException('foo'));

        $this->getProvider()->authenticate($token);
    }

    /**
     * @test
     * @expectedException \Thrustbit\Security\Domain\User\Exceptions\UserStatusException
     */
    public function it_raise_exception_when_user_status_failed_on_post_authentication(): void
    {
        $user = $this->getMockForAbstractClass(LocalUser::class);
        $this->userProvider->expects($this->once())->method('requireByIdentifier')->willReturn($user);
        $this->encoder->expects($this->once())->method('check')->willReturn(true);

        $token = $this->getFakeLoginToken();

        $this->userChecker->expects($this->once())->method('onPostAuthentication')
            ->willThrowException(new UserStatusException('foo'));

        $this->getProvider()->authenticate($token);
    }

    /**
     * @test
     * @expectedException \Thrustbit\Security\Domain\User\Exceptions\UserNotFound
     */
    public function it_raise_exception_and_hide_raising_bad_credential_exception(): void
    {
        $user = $this->getMockForAbstractClass(LocalUser::class);
        $this->userProvider->expects($this->once())->method('requireByIdentifier')->willReturn($user);
        $this->encoder->expects($this->never())->method('check');

        $token = $this->getFakeLoginTokenWithEmptyCredential();

        $this->hideException = true;
        $this->getProvider()->authenticate($token);
    }

    protected function getProvider(): UserNamePasswordAuthenticationProvider
    {
        return new UserNamePasswordAuthenticationProvider(
            $this->providerKey,
            $this->userProvider,
            $this->userChecker,
            $this->encoder,
            $this->hideException
        );
    }

    protected function setUpMock(): void
    {
        $this->token = $this->getMockForAbstractClass(Tokenable::class);
        $this->userProvider = $this->getMockForAbstractClass(UserProvider::class);
        $this->userChecker = $this->getMockForAbstractClass(SecurityUserChecker::class);
        $this->encoder = $this->getMockForAbstractClass(Hasher::class);
        $this->providerKey = new ProviderKey('foo');
    }

    protected function getFakeLoginToken(array $roles = []): UserNamePasswordToken
    {
        return new UserNamePasswordToken(
            new SomeUserIdentifier(true),
            SimpleCredentialFactory::fromString('password'),
            $this->providerKey,
            $roles
        );
    }

    protected function getFakeLoginTokenWithEmptyCredential(): UserNamePasswordToken
    {
        return new UserNamePasswordToken(
            new SomeUserIdentifier(true),
            new EmptyCredential,
            $this->providerKey
        );
    }

    protected function getUserNamePasswordToken(string $providerKey = null): UserNamePasswordToken
    {
        $id = EmailAddress::fromString('foo@bar.com');
        $credential = SimpleCredentialFactory::fromString('password');

        if ($providerKey) {
            $providerKey = new ProviderKey($providerKey);
        }

        return new UserNamePasswordToken($id, $credential, $providerKey ?? $this->providerKey, []);
    }

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Tokenable
     */
    private $token;

    /**
     * @var ProviderKey
     */
    private $providerKey;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|UserProvider
     */
    private $userProvider;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|SecurityUserChecker
     */
    private $userChecker;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Hasher
     */
    private $encoder;

    /**
     * @var bool
     */
    private $hideException = false;
}