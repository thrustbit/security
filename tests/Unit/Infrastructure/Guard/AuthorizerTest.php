<?php

declare(strict_types=1);

namespace ThrustbitTests\Security\Unit\Infrastructure\Guard;

use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;
use Thrustbit\Security\Infrastructure\Guard\Authorization\Contract\Authorizable;
use Thrustbit\Security\Infrastructure\Guard\Authorizer;
use ThrustbitTests\Security\TestCase;

class AuthorizerTest extends TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Authorizable
     */
    private $authorizable;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Tokenable
     */
    private $token;

    public function setUp()
    {
        parent::setUp();

        $this->authorizable = $this->getMockForAbstractClass(Authorizable::class);

        $this->token = $this->getMockForAbstractClass(Tokenable::class);
    }

    /**
     * @test
     */
    public function it_grant_access(): void
    {
        $this->authorizable->expects($this->once())->method('decide')->willReturn(true);

        $auth = $this->getAuthorizer();

        $this->assertTrue($auth->grant($this->token, []));
    }

    /**
     * @test
     */
    public function it_grant_required_access(): void
    {
        $this->authorizable->expects($this->once())->method('decide')->willReturn(true);

        $auth = $this->getAuthorizer();

        $this->assertTrue($auth->requireGranted($this->token, []));
    }

    /**
     * @test
     */
    public function it_deny_access(): void
    {
        $this->authorizable->expects($this->once())->method('decide')->willReturn(false);

        $auth = $this->getAuthorizer();

        $this->assertFalse($auth->grant($this->token, []));
    }

    /**
     * @test
     * @expectedException \Thrustbit\Security\Application\Exceptions\AuthorizationException
     */
    public function it_raise_exception_when_granted_access_is_required(): void
    {
        $this->authorizable->expects($this->once())->method('decide')->willReturn(false);

        $auth = $this->getAuthorizer();

        $auth->requireGranted($this->token, []);
    }

    private function getAuthorizer(): Authorizer
    {
        return new Authorizer($this->authorizable);
    }
}