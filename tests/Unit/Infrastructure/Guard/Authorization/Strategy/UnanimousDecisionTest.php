<?php

declare(strict_types=1);

namespace ThrustbitTests\Security\Unit\Infrastructure\Guard\Authorization\Strategy;

use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;
use Thrustbit\Security\Infrastructure\Guard\Authorization\Contract\Votable;
use Thrustbit\Security\Infrastructure\Guard\Authorization\Strategy\UnanimousDecision;
use ThrustbitTests\Security\TestCase;

class UnanimousDecisionTest extends TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Tokenable
     */
    private $token;

    public function setUp()
    {
        $this->token = $this->getMockForAbstractClass(Tokenable::class);
    }

    /**
     * @test
     */
    public function it_grant_access(): void
    {
        $grantVoter = $this->vote(true, true);

        $this->assertTrue($this->getDecision([$grantVoter], ['ROLE_FOO']));
    }

    /**
     * @test
     */
    public function it_deny_access(): void
    {
        $voter = $this->vote(false, true);

        $this->assertFalse($this->getDecision([$voter], ['ROLE_FOO']));
    }

    /**
     * @test
     */
    public function it_grant_access_when_all_abstain(): void
    {
        $abstainVoter = $this->getMockForAbstractClass(Votable::class);
        $abstainVoter->expects($this->once())->method('vote')->willReturn(0);

        $this->assertTrue($this->getDecision([$abstainVoter], ['ROLE_FOO'], TRUE));
    }

    /**
     * @test
     */
    public function it_deny_access_when_all_abstain(): void
    {
        $abstainVoter = $this->getMockForAbstractClass(Votable::class);
        $abstainVoter->expects($this->once())->method('vote')->willReturn(0);

        $this->assertFalse($this->getDecision([$abstainVoter], ['ROLE_FOO'], FALSE));
    }

    public function it_vote_on_every_voter_per_attribute(): void
    {
        $grantVoter = $this->getMockForAbstractClass(Votable::class);
        $grantVoter->expects($this->exactly(2))->method('vote')->willReturn(+1);

        $this->assertTrue($this->getDecision([$grantVoter], ['ROLE_FOO', 'ROLE_BAR']));
    }

    /**
     * @test
     */
    public function it_denied_access_on_first_fail_voter(): void
    {
        $deniedVoter = $this->vote(false, true);
        $grantVoter = $this->vote(true,false);

        $this->assertFalse($this->getDecision([$deniedVoter, $grantVoter], ['ROLE_VOTER']));
    }

    /**
     * @test
     */
    public function it_denied_access_on_first_fail_voter_2(): void
    {
        $grantVoter = $this->vote(true, true);
        $deniedVoter = $this->vote(false,true);
        $grantVoter1 = $this->vote(true, false);

        $this->assertFalse($this->getDecision([$grantVoter, $deniedVoter, $grantVoter1], ['ROLE_VOTER']));
    }

    private function getDecision(array $voters, array $attributes, bool $allowIfAllAbstain = false): bool
    {
        $decision = new UnanimousDecision($voters, $allowIfAllAbstain);

        return $decision->decide($this->token, $attributes, new \stdClass());
    }

    private function vote(bool $grant, bool $touch): Votable
    {
        $voter = $this->getMockForAbstractClass(Votable::class);
        $vote = $grant ? 1 : -1;
        if ($touch) {
            $voter->expects($this->once())->method('vote')->willReturn($vote);
        } else {
            $voter->expects($this->never())->method('vote');
        }

        return $voter;
    }
}