<?php

declare(strict_types=1);

namespace ThrustbitTests\Security\Unit\Infrastructure\Guard\Authorization;

use Thrustbit\Security\Application\Exceptions\Service\CredentialsNotFound;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;
use Thrustbit\Security\Infrastructure\Guard\Authorization\AuthorizationChecker;
use Thrustbit\Security\Infrastructure\Guard\Authorization\Contract\Authorizable;
use Thrustbit\Security\Infrastructure\Guard\Guard;
use ThrustbitTests\Security\TestCase;

class AuthorizationCheckerTest extends TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Authorizable
     */
    private $strategy;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Tokenable
     */
    private $token;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Guard
     */
    private $guard;

    public function setUp()
    {
        parent::setUp();

        $this->strategy = $this->getMockForAbstractClass(Authorizable::class);
        $this->token = $this->getMockForAbstractClass(Tokenable::class);
        $this->guard = $this->getMockBuilder(Guard::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();
    }

    /**
     * @test
     */
    public function it_grant_access(): void
    {
        $this->guard->expects($this->once())->method('requireToken')->willReturn($this->token);
        $this->token->expects($this->once())->method('isAuthenticated')->willReturn(true);
        $this->guard->expects($this->never())->method('authenticate');
        $this->guard->expects($this->never())->method('setToken');

        $this->strategy->expects($this->once())->method('decide')->willReturn(true);

        $this->assertTrue($this->getAuthChecker()->isGranted(['ROLE_FOO']));
    }

    /**
     * @test
     */
    public function it_deny_access(): void
    {
        $this->guard->expects($this->once())->method('requireToken')->willReturn($this->token);
        $this->token->expects($this->once())->method('isAuthenticated')->willReturn(true);
        $this->guard->expects($this->never())->method('authenticate');
        $this->guard->expects($this->never())->method('setToken');

        $this->strategy->expects($this->once())->method('decide')->willReturn(false);

        $this->assertFalse($this->getAuthChecker()->isGranted(['ROLE_FOO']));
    }

    /**
     * @test
     * @expectedException \Thrustbit\Security\Application\Exceptions\Service\CredentialsNotFound
     */
    public function it_raise_exception_when_token_storage_is_empty()
    {
        $exception = new CredentialsNotFound('foo');
        $this->guard->expects($this->once())->method('requireToken')->willThrowException($exception);

        $auth = $this->getAuthChecker();
        $auth->isGranted(['ROLE_FOO']);
    }

    /**
     * @test
     */
    public function it_authenticate_token_when_token_is_not_authenticated(): void
    {
        $this->guard->expects($this->once())->method('requireToken')->willReturn($this->token);
        $this->token->expects($this->once())->method('isAuthenticated')->willReturn(false);
        $this->guard->expects($this->once())->method('authenticate')->willReturn($this->token);
        $this->guard->expects($this->once())->method('setToken');

        $this->strategy->expects($this->once())->method('decide')->willReturn(true);

        $this->assertTrue($this->getAuthChecker()->isGranted(['ROLE_FOO']));
    }

    /**
     * @test
     */
    public function it_can_force_re_authentication_of_token(): void
    {
        $this->guard->expects($this->once())->method('requireToken')->willReturn($this->token);
        $this->token->expects($this->never())->method('isAuthenticated');
        $this->guard->expects($this->once())->method('authenticate')->willReturn($this->token);
        $this->guard->expects($this->once())->method('setToken');

        $this->strategy->expects($this->once())->method('decide')->willReturn(true);

        $forceAuthentication = true;
        $this->assertTrue($this->getAuthChecker($forceAuthentication)->isGranted(['ROLE_FOO']));
    }

    /**
     * @test
     */
    public function it_can_force_re_authentication_of_token_2(): void
    {
        $this->guard->expects($this->once())->method('requireToken')->willReturn($this->token);
        $this->token->expects($this->never())->method('isAuthenticated');
        $this->guard->expects($this->once())->method('authenticate')->willReturn($this->token);
        $this->guard->expects($this->once())->method('setToken');

        $this->strategy->expects($this->once())->method('decide')->willReturn(true);

        $auth = $this->getAuthChecker(FALSE);
        $auth->forceAuthentication(TRUE);

        $this->assertTrue($auth->isGranted(['ROLE_FOO']));
    }

    private function getAuthChecker(bool $forceAuthentication = false): AuthorizationChecker
    {
        return new AuthorizationChecker($this->guard, $this->strategy, $forceAuthentication);
    }
}