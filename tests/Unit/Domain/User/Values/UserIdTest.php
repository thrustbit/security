<?php

declare(strict_types=1);

namespace ThrustbitTests\Security\Unit\Domain\User\Values;

use Thrustbit\Security\Domain\User\Values\UserId;
use ThrustbitTests\Security\TestCase;

class UserIdTest extends TestCase
{
    /**
     * @test
     */
    public function it_return_different_unique_id(): void
    {
        $uid = UserId::nextIdentity();
        $newUid = UserId::nextIdentity();

        $this->assertNotEquals($uid, $newUid);
        $this->assertNotSame($uid, $newUid);
        $this->assertFalse($uid->sameValueAs($newUid));
        $this->assertNotEquals((string)$uid, (string)$newUid);
    }
}