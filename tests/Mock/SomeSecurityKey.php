<?php

declare(strict_types=1);

namespace ThrustbitTests\Security\Mock;

use Thrustbit\Security\Application\Values\SecurityKey\SecurityKey;
use Thrustbit\Security\Application\Values\SecurityValue;

class SomeSecurityKey extends SecurityKey
{
    public function sameValueAs(SecurityValue $aValue): bool
    {
        return $aValue instanceof $this
            && $this->getKey() === $aValue->getKey();
    }
}