<?php

declare(strict_types=1);

namespace ThrustbitTests\Security\Mock;

use Thrustbit\Security\Application\Values\EmailAddress;
use Thrustbit\Security\Application\Values\EncodedPassword;
use Thrustbit\Security\Application\Values\Name;
use Thrustbit\Security\Application\Values\SecurityIdentifier;
use Thrustbit\Security\Application\Values\Uid;
use Thrustbit\Security\Domain\User\LocalUser;
use Thrustbit\Security\Domain\User\Values\BcryptPassword;
use Thrustbit\Security\Domain\User\Values\UserId;
use Thrustbit\Security\Domain\User\Values\UserName;

class Account implements LocalUser
{
    /**
     * @var Uid
     */
    private $userId;

    public function __construct()
    {
        $this->userId = UserId::nextIdentity();
    }

    public function getPassword(): EncodedPassword
    {
        return new BcryptPassword(password_hash('password', PASSWORD_BCRYPT));
    }

    public function getId(): SecurityIdentifier
    {
        return $this->userId;
    }

    public function getIdentifier(): SecurityIdentifier
    {
        return $this->getId();
    }

    public function getEmail(): EmailAddress
    {
        return \Thrustbit\Security\Domain\User\Values\EmailAddress::fromString('some@email.com');
    }

    public function getUserName(): Name
    {
        return UserName::fromString('username');
    }

    public function getRoles(): array
    {
        return ['ROLE_USER'];
    }
}