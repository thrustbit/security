<?php

declare(strict_types=1);

namespace ThrustbitTests\Security\Mock;

use Thrustbit\Security\Domain\User\UserThrottle;

class AccountThrottle extends Account implements UserThrottle
{
    public function isEnabled(): bool
    {
        return true;
    }

    public function isNonLocked(): bool
    {
        return true;
    }
}