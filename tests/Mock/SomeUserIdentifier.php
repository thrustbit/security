<?php

declare(strict_types=1);

namespace ThrustbitTests\Security\Mock;

use Thrustbit\Security\Application\Values\SecurityIdentifier;
use Thrustbit\Security\Application\Values\SecurityValue;

class SomeUserIdentifier implements SecurityIdentifier
{
    /**
     * @var bool
     */
    private $sameValue;

    public function __construct(bool $sameValue)
    {
        $this->sameValue = $sameValue;
    }

    public function read()
    {
        return 'foo';
    }

    public function sameValueAs(SecurityValue $aValue): bool
    {
        return $this->sameValue;
    }

    public function __toString(): string
    {
        return $this->read();
    }
}