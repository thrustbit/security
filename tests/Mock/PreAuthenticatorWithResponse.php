<?php

declare(strict_types=1);

namespace ThrustbitTests\Security\Mock;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Application\Exceptions\AuthenticationException;
use Thrustbit\Security\Application\Http\Response\Authentication\Contract\AuthenticationFailure;
use Thrustbit\Security\Application\Http\Response\Authentication\Contract\AuthenticationSuccess;
use Thrustbit\Security\Application\Values\SecurityKey;
use Thrustbit\Security\Domain\User\Provider\UserProvider;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\SimplePreAuthenticator;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\PreAuthenticatedToken;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Values\SimpleCredentialFactory;

class PreAuthenticatorWithResponse implements SimplePreAuthenticator, AuthenticationSuccess, AuthenticationFailure
{
    /**
     * @var bool
     */
    private $supportToken;

    /**
     * @var bool
     */
    private $exceptionOnAuthentication;

    public function __construct(bool $supportToken = true, bool $exceptionOnAuthentication = false)
    {
        $this->supportToken = $supportToken;
        $this->exceptionOnAuthentication = $exceptionOnAuthentication;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception = null): Response
    {
        return new Response('failure');
    }

    public function onAuthenticationSuccess(Request $request, Tokenable $token): Response
    {
        return new Response('success');
    }

    public function authenticateToken(Tokenable $token, UserProvider $userProvider, SecurityKey $securityKey): Tokenable
    {
        if (!$this->exceptionOnAuthentication) {
            return $token;
        }

        throw new AuthenticationException('bar');
    }

    public function supportsToken(Tokenable $token, SecurityKey $securityKey): bool
    {
        return $this->supportToken;
    }

    public function createToken(Request $request, SecurityKey $securityKey): Tokenable
    {
        return new PreAuthenticatedToken(
            new SomeUserIdentifier(true),
            SimpleCredentialFactory::fromString('password'),
            $securityKey
        );
    }
}