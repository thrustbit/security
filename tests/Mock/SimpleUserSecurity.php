<?php

declare(strict_types=1);

namespace ThrustbitTests\Security\Mock;

use Illuminate\Contracts\Support\Jsonable;
use Thrustbit\Security\Application\Values\EmailAddress;
use Thrustbit\Security\Application\Values\EncodedPassword;
use Thrustbit\Security\Application\Values\Name;
use Thrustbit\Security\Application\Values\SecurityIdentifier;
use Thrustbit\Security\Domain\Role\Values\RoleValue;
use Thrustbit\Security\Domain\User\LocalUser;
use Thrustbit\Security\Domain\User\Values\BcryptPassword;
use Thrustbit\Security\Domain\User\Values\EmailAddress as Email;
use Thrustbit\Security\Domain\User\Values\UserId;
use Thrustbit\Security\Domain\User\Values\UserName;

class SimpleUserSecurity implements LocalUser, \JsonSerializable, Jsonable
{
    private $id;
    private $userName;
    private $email;
    private $password;
    private $roles = [];

    public function __construct(array $roles = ['ROLE_FOO'])
    {
        $this->id = UserId::nextIdentity();
        $this->userName = UserName::fromString('foo_bar');
        $this->email = Email::fromString('foo@bar.com');
        $encodedPassword = password_hash('password', PASSWORD_BCRYPT);
        $this->password = new BcryptPassword($encodedPassword);

        foreach ($roles as $role){
            $this->roles []= new RoleValue($role);
        }
    }

    public function getPassword(): EncodedPassword
    {
        return $this->password;
    }

    public function getId(): SecurityIdentifier
    {
        return $this->id;
    }

    public function getIdentifier(): SecurityIdentifier
    {
        return $this->id;
    }

    public function getEmail(): EmailAddress
    {
        return $this->email;
    }

    public function getUserName(): Name
    {
        return $this->userName;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function toArray()
    {
        return [
            'id' => $this->id->read(),
            'email' => $this->email->read(),
            'user_name' => $this->userName->read(),
            'password' => $this->password->getCredential(),
            'roles' => $this->roles
        ];
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }

    public function toJson($options = 0)
    {
        return json_encode($this->jsonSerialize());
    }

    public function __toString(): string
    {
        return $this->toJson();
    }
}